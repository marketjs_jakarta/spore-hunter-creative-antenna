#
# MarketJS Deployment System
# -----------------------------------------------------------------------
# Copyright (c) 2012 MarketJS Limited. Certain portions may come from 3rd parties and
# carry their own licensing terms and are referenced where applicable.
# -----------------------------------------------------------------------

#! /bin/bash
# Usage: bash push.sh [options]
# Example: bash push.sh -b -d (bake, then deploy)
VERSION="1.0.0"

# Configurations
BITBUCKET_BRANCH="master"
ENABLE_FRAMEBREAKER=true
ENABLE_COPYRIGHT=true
INCLUDE_VCONSOLE=false
REMOVE_TEST_AD=true

# Variables
CURRENT_DIRECTORY=${PWD}/

# Methods
bake (){
    echo ""
    echo "Baking ..."
    echo ""

    cd tools
    bash bake.sh
    cd ..

    echo ""
    echo "Baking Done!"
    echo ""
}

compile_test_game (){
    echo "Compiling game.js for testing ..."
    java -jar compiler.jar \
    --warning_level=QUIET \
    --js=media/text/strings.js \
    --js=settings/dev.js \
    --js=settings/ad/mobile/preroll/themes/light/ad.js \
    --js=settings/ad/mobile/header/themes/light/ad.js \
    --js=settings/ad/mobile/footer/themes/light/ad.js \
    --js=settings/ad/mobile/end/themes/light/ad.js \
    --js=_factory/game/game.js \
    --js_output_file=game.js \
    --language_in=ECMASCRIPT5
    echo "Done!"

    echo "Compiling game.css for testing ..."
    bash css-append.sh
    bash css-minify.sh temp.css game.css
    sed -i.bak 's/..\/..\/..\/..\/..\/..\///g' game.css
    rm temp.css
    rm *.bak

    echo "Done!"
}

prep_production (){
    echo "Zipping up media files for target language ..."

    #echo '$1:' $1
    #echo '$2:' $2
    #echo '$3:' $3
    #echo '$4:' $4

    #bash zip-media-folder.sh $1
    #echo "Done ..."

    echo "Create basic index.html ..."
    cp dev.html index.html
    echo "Done ..."

    echo "Cleaning up paths ..."
    # Clean CSS paths
    sed -n '/settings\/ad\/mobile\/preroll\/themes\/light\/ad.css/!p' index.html > temp && mv temp index.html
    sed -n '/settings\/ad\/mobile\/header\/themes\/light\/ad.css/!p' index.html > temp && mv temp index.html
    sed -n '/settings\/ad\/mobile\/footer\/themes\/light\/ad.css/!p' index.html > temp && mv temp index.html
    sed -n '/settings\/ad\/mobile\/end\/themes\/light\/ad.css/!p' index.html > temp && mv temp index.html
    sed -n '/settings\/debug\/debug.css/!p' index.html > temp && mv temp index.html
    sed -i.bak 's/main.css/game.css/g' index.html

    # Clean JS paths
    sed -n '/glue\/jquery\/jquery-3.2.1.min.js/!p' index.html > temp && mv temp index.html
    sed -i.bak 's/glue\/load\/load.js/game.js/g' index.html

    # Remove temp files
    echo "Removing temp files ..."
    rm *.bak
    rm temp
    echo "Done!"

    # Transfer to _factory    
    if [ ! -d ./_factory ]; then
        echo "_factory not found!"
    else
        echo "_factory exist. Removing"
        rm -r ./_factory
    fi;
        
    mkdir -p ./_factory;
    mkdir -p ./_factory/game;
    
    if [ "$REMOVE_TEST_AD" = true ] ; 
    then
        sed '/<!-- AdTest-MobileAdInGamePreroll -->/,/<!-- EndOfAdTest-MobileAdInGamePreroll -->/d' index.html > index.html.temp
        mv index.html.temp index.html
    fi

    echo "Compiling game.js for _factory ..."
    if [ "$INCLUDE_VCONSOLE" = true ] ; 
    then
        java -jar compiler.jar \
        --warning_level=QUIET \
        --js=glue/debug/vconsole.min.js \
        --js=glue/debug/vconsole.init.js \
        --js=glue/jquery/jquery-3.2.1.min.js \
        --js=glue/ie/ie.js \
        --js=glue/jukebox/Player.js \
        --js=glue/howler/howler.js \
        --js=glue/font/promise.polyfill.js \
        --js=glue/font/fontfaceobserver.standalone.js \
        --js=game.min.js \
        --js_output_file=_factory/game/game.js \
        --language_in=ECMASCRIPT5
    else
        java -jar compiler.jar \
        --warning_level=QUIET \
        --js=glue/jquery/jquery-3.2.1.min.js \
        --js=glue/ie/ie.js \
        --js=glue/jukebox/Player.js \
        --js=glue/howler/howler.js \
        --js=glue/font/promise.polyfill.js \
        --js=glue/font/fontfaceobserver.standalone.js \
        --js=game.min.js \
        --js_output_file=_factory/game/game.js \
        --language_in=ECMASCRIPT5
    fi
    echo "Done!"

    # Remove temp files
    echo "Removing game.min.js ..."
    rm game.min.js
    echo "Done!"
}

while getopts "bh" opt; do
  case $opt in
    h)
      echo "Usage: sh push.sh [option]"
       echo "Deploy Options"
       echo "\t -b \t Build all files"
       echo "Working example (copy paste directly): sh push.sh -b"
      ;;
    b)
       bake
       prep_production
       compile_test_game
      ;;
    \?)
       echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
