sh push.sh -b

ARCHIVE_NAME=${PWD##*/}""

echo "Building archive ..."

if [ ! -f ./$ARCHIVE_NAME.zip ];
then
    echo "File not found!"
else
	echo "File exist. Removing"
rm ./$ARCHIVE_NAME.zip
fi

zip -r ./$ARCHIVE_NAME.zip ./index.html ./game.js ./game.css ./media -x "*.zip*" -x "*.git*" -x "*.psd*" -x "*.xcf*" -x "*.aif*" -x "*.tiff*" -x "*.au*" -x "*.txt*" -x "*.bat*" -x "*.jar*" -x "*.py*" -x "*.sh*" -x "*.php*" -x "*.htaccess" -x "*.DS_Store"

echo "Done"