ig.module('game.entities.controllers.menu-controller')
  .requires(
    'game.entities.menu.game-title',
    'game.entities.buttons.button-play',
    'game.entities.buttons.button-setting',
    'game.entities.buttons.button-more-games',
    'game.entities.common.pop-up-settings',
    'game.entities.game.game-background',
    'game.entities.kvn-delay-execution'
  )
  .defines(function () {
    EntityMenuController = ig.Entity.extend({
      zIndex: -10,
      size: { x: 540, y: 960 },
      animSheet: new ig.AnimationSheet('media/graphics/sprites/menu/background-menu.png', 540, 960),

      // define global variables
      rWidth: null,
      rHeight: null,
      rHeightH: null,
      rWidthH: null,

      // logic variables
      menuOpened: "mainmenu", // ["mainmenu", "settings"]
      menuList: ["mainmenu", "settings"],

      init: function (x, y, settings) {
        this.parent(x, y, settings);

        // Set background
        this.addAnim('idle', 1, [0]);

        ig.game.gravity = 0;

        if (!ig.global.wm) {
          // set global variable
          this.rWidth = ig.system.realWidth;
          this.rHeight = ig.system.realHeight;
          this.rHeightH = this.rHeight / 2;
          this.rWidthH = this.rWidth / 2;

          // Creating entities
          var titlePosY = this.rHeight * 0.3;
          var btnPlayPosY = this.rHeight * 0.6;
          var btnSettingsPosY = this.rHeight * 0.85;
          var btnMoreGamesPosY = this.rHeight * 0.85;

          // Spawn Title
          this.title = ig.game.spawnEntity(EntityGameTitle, 0, 0, { controller: this });
          this.title.setMiddlePosition(this.rWidthH, titlePosY);

          // Spawn Play Button
          this.btnPlay = ig.game.spawnEntity(EntityButtonPlay, 0, 0, { controller: this });
          this.btnPlay.setMiddlePosition(this.rWidthH, btnPlayPosY);

          // If more games button enabled then spawn button settings and button more games side by side
          if (_SETTINGS.MoreGames.Enabled) {
            // // Spawn Settings Button
            this.btnSettings = ig.game.spawnEntity(EntityButtonSetting, 0, 0, { controller: this });
            this.btnSettings.setMiddlePosition(this.rWidth * 0.25, btnSettingsPosY);

            // Spawn More Games Button
            this.btnMoreGames = ig.game.spawnEntity(EntityButtonMoreGames, 0, 0, { controller: this });
            this.btnMoreGames.setMiddlePosition(this.rWidth * 0.75, btnMoreGamesPosY);
            this.btnMoreGames.setButtonLink();
          } 
          // If more games button disabled then spawn button settings in the middle
          else {
            // Spawn Settings Button
            this.btnSettings = ig.game.spawnEntity(EntityButtonSetting, 0, 0, { controller: this });
            this.btnSettings.setMiddlePosition(this.rWidthH, btnSettingsPosY);
          }

          // Spawn Pop Up Settings
          this.popUpSettings = ig.game.spawnEntity(EntityPopUpSettings, this.rWidthH, this.rHeightH, { controller: this });

          this.mainMenuIn();

          // Spawn Screen Pointer (handles click and touch)
          ig.game.spawnEntity(EntityPointerExtended, 0, 0);

          // Fullscreen button
          var btn = ig.game.spawnEntity(ig.FullscreenButton, 5, 5, {
            enterImage: new ig.Image("media/graphics/misc/button-fullscreen.png"),
            exitImage: new ig.Image("media/graphics/misc/button-minimize.png")
          });
        
        }
      },

      // Main Menu Animation

      mainMenuIn: function( callback ){
        // Reset object state
        var titleEndPos = new Vector2(this.title.pos.x, this.title.pos.y);
        this.title.pos.y = -270;

        this.btnPlay.pos.y = -1000;
        this.btnSettings.pos.y = -1000;

        if(this.btnMoreGames != null){
          this.btnMoreGames.pos.y = -1000;
        }

        // Show title
        this.title.tween({ pos: { x: titleEndPos.x, y: titleEndPos.y } }, 1, {
          easing: ig.Tween.Easing.Back.EaseOut,
          onComplete: function () {
            this.btnPlay.show(0.5, function(){          // Show btnPlay after title
              this.btnSettings.show(0.5, function(){    // Show btnSettings after btnPlay
                if(this.btnMoreGames != null){
                  this.btnMoreGames.show(0.5, function(){ // Show btnMoreGames after btnSettings
                    callback ? callback() : "";
                  });
                }
              }.bind(this));
            }.bind(this));
          }.bind(this)
        }).start();
      },

      mainMenuOut: function( callback ){
        this.title.tween({ pos: { x: this.pos.x, y: -270 } }, 1, {
          easing: ig.Tween.Easing.Back.EaseIn,
          onComplete: function () {
            // var gameBgScroll = ig.game.spawnEntity(EntityGameBackground, 0, 0, {zIndex: this.zIndex, stopPosBefore: 0, currentTargetStop: 1, scrollTime: 1, controller: this});
            ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
              .setTimeOut(0.2, function(){
                // Go to the game
                ig.game.gotoGame();
              });
          }.bind(this)
        }).start();
        this.btnPlay.hide();
        this.btnSettings.hide();

        if(this.btnMoreGames != null){
          this.btnMoreGames.hide();
        }
      },

      // Setings panel
      
      openSettings: function(){
        this.menuOpened = this.menuList[1];
        this.popUpSettings.easeIn();
        this.btnPlay.disableClick();
        this.btnSettings.disableClick();

        if(this.btnMoreGames != null){
          this.btnMoreGames.disableClick();
        }
      },

      closeSettings: function(){
        this.menuOpened = this.menuList[0];
        this.popUpSettings.easeOut();
        this.btnPlay.enableClick();
        this.btnSettings.enableClick();

        if(this.btnMoreGames != null){
          this.btnMoreGames.enableClick();
        }
      },

      // Menu buttons
      onBtnPlayClicked: function(){
        this.mainMenuOut();
      }
    });
  });
