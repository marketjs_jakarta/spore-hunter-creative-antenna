ig.module('game.entities.controllers.game-controller')
.requires(
  'game.entities.others.marketjs-entity',
  'game.entities.game.game-background',
  'game.entities.buttons.button-pause',
  'game.entities.buttons.button-move',
  'game.entities.game.player',
  'game.entities.game.enemy',
  'game.entities.game.enemy-special',
  'game.entities.game.enemy-green',
  'game.entities.game.enemy-orange',
  'game.entities.game.enemy-red',
  'game.entities.game.drone',
  'game.entities.game.pop-up-gameover',
  'game.entities.game.game-level',
  'game.entities.game.game-ui',
  'game.entities.game.tutorial',

  'game.entities.kvn-delay-execution',

  'plugins.kvn-utility'
)
.defines(function() {
  EntityGameController = ig.Entity.extend({
    zIndex:-10,
    size: {x:540, y:960},
    // animSheet: new ig.AnimationSheet('media/graphics/sprites/game/background-game.png',540, 960),

    // Game properties
    isGameOver: false,
    isGamePause: false,
    timerGame: null,
    timerSpawnEnemy: null,
    timerSpawnDrone: null,
    timerSpawnHazard: null,
    enemySpawnInterval: 3,
    droneSpawnInterval: 20,
    hazardSpawnInterval: 30,
    score: 0,

    // Tutorial
    playerMovedOnce: false,   // After player moved once (finish tutorial)

    // define global variables
    rWidth: null,
    rHeight: null,
    rHeightH: null,
    rWidthH: null,
    groundPosY: null,
    enemyVariation:
    {
      small: { sizeIndex: 0, name: "small", scale: 0.6, fontSize: 30 },
      medium: { sizeIndex: 1, name: "medium", scale: 0.9, fontSize: 45 },
      big: { sizeIndex: 2, name: "big", scale: 1.2, fontSize: 60 },
      large: { sizeIndex: 3, name: "large", scale: 1.7, fontSize: 85 },
    },

    enemyVariationArray: [],  // Array version of enemyVariation object

    // Level Progression
    wave: 1,
    levelManager: null,
    maxEnemySpawned: 6,   // Not spawning more enemy if arrayEnemyInStage more than this number
    arrayEnemyInStage: null,
    // ---------------------------------

    init: function( x, y, settings ) {
      this.parent( x, y, settings );

      // Set background
      // this.addAnim('idle' , 1, [0]);

      this.timerGame = new ig.Timer();
      this.timerSpawnEnemy = new ig.Timer();
      this.timerSpawnDrone = new ig.Timer();
      this.timerSpawnHazard = new ig.Timer();

      ig.game.gravity = 300;

      if(!ig.global.wm){
        // set global variable
        this.rWidth = ig.system.realWidth;
        this.rHeight = ig.system.realHeight;
        this.rHeightH = this.rHeight / 2;
        this.rWidthH = this.rWidth / 2;
        this.groundPosY = this.rHeight - 70;

        // Create entities

        // Spawn background
        this.bg1 = ig.game.spawnEntity(EntityGameBackground, 0, 0, {zIndex: this.zIndex, stopPosBefore: 0, currentTargetStop: 1, controller: this});
        this.bg2 = ig.game.spawnEntity(EntityGameBackground, 0, 0, {zIndex: this.zIndex, stopPosBefore: 1, currentTargetStop: 2, controller: this});

        // Spawn Player
        this.player = ig.game.spawnEntity(EntityPlayer, this.rWidthH - 31, this.rHeight - 125, {controller: this});

        // Spawn Button Move
        this.btnMove = ig.game.spawnEntity(EntityButtonMove, 0, 0, {controller: this});

        // Delayed Spawn Tutorial (matching another animation timing)
        ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
              .setTimeOut(1.5, function(){
                this.tutorial = ig.game.spawnEntity(EntityTutorial, this.rWidthH, this.rHeightH + 160, { controller: this });
              }.bind(this));

        // Spawn Pause Button
        this.btnPause = ig.game.spawnEntity(EntityButtonPause, 10, 10, { controller: this });
        this.btnPause.pos.x = this.btnPause.pos.x - this.btnPause.size.x - 20;
        ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
              .setTimeOut(1, function(){
                this.btnPause.show();
              }.bind(this));

        // Spawn Pop Up Settings
        this.popUpSettings = ig.game.spawnEntity(EntityPopUpSettings, this.rWidthH, this.rHeightH, { controller: this });
        
        // Spawn Pop Up Gameover
        this.popUpGameover = ig.game.spawnEntity(EntityPopUpGameover, this.rWidthH, this.rHeightH, { controller: this });

        // Spawn game level
        this.levelManager = ig.game.spawnEntity(EntityGameLevel, this.rWidth, this.rHeight, { controller: this });

        // Spawn Game UI
        ig.game.spawnEntity(EntityGameUi, 0, 0, { controller: this });

        // Spawn Screen Pointer (handles click and touch)
        ig.game.spawnEntity(EntityPointerExtended, 0, 0);

        this.enemyVariationArray = [
          this.enemyVariation.small,
          this.enemyVariation.medium,
          this.enemyVariation.big,
          this.enemyVariation.large
        ];

        this.arrayEnemyInStage = [];
      }
    },

    update: function(){
      // Spawning enemies
      if(!this.isGamePause && !this.isGameOver){
        if(this.timerSpawnEnemy.delta() > this.enemySpawnInterval && this.canSpawnEnemy()){
          this.updateEnemies();
          var config = this.levelManager.getEnemiesConfig();

          var enemyHealth = ig.KvnUtility.getRandomInteger(config.minHealth, config.maxHealth);

          var randVelX = ig.KvnUtility.getRandomInteger(20, 30);
          var randSpawnPos = ig.KvnUtility.getRandomInteger(0, 2);  // 0 is left, otherwise right

          if(randSpawnPos == 0){
              // Spawn left
              var posX = this.rWidth * 0.25;
              settings = {vel: {x: randVelX, y: 0}, health: enemyHealth, variation: config.enemySize, controller: this};
              var en = ig.game.spawnEntity(config.enemyEntity, this.rWidthH / 2, 0, settings);
              en.setTopMiddlePosition(posX, -en.size.y - 10);
          }
          else{
              // Spawn right
              var posX = this.rWidth * 0.75;
              settings = {vel: {x: -randVelX, y: 0}, health: enemyHealth, variation: config.enemySize, controller: this};
              var en = ig.game.spawnEntity(config.enemyEntity, this.rWidthH + this.rWidthH / 2, 0, settings);
              en.setTopMiddlePosition(posX, -en.size.y - 10);
          }

          this.timerSpawnEnemy.reset();
        }

        // Spawning drones
        if(this.timerSpawnDrone.delta() > this.droneSpawnInterval){
          var settings = {controller: this};
          ig.game.spawnEntity(EntityDrone, -100, 200, settings);
          this.timerSpawnDrone.reset();
        }

        // Spawning hazard
        if(this.timerSpawnHazard.delta() > this.hazardSpawnInterval && this.player.powerLevel == 3){
          var randSpawnPos = ig.KvnUtility.getRandomInteger(0, 2);  // 0 is left, otherwise right
          var posX = 0;
          var velX = 0;
          if(randSpawnPos == 0){
            posX = 0;
            velX = 50;
          }else{
            posX = this.rWidth;
            velX = -50;
          }
          var settings = { vel: {x: velX, y: 0}, controller: this};
          var haz = ig.game.spawnEntity(EntityEnemySpecial, posX, 0, settings);
          haz.setTopMiddlePosition(posX, -haz.size.y - 10);

          this.timerSpawnHazard.reset();
        }
      }
    },

    // Game logic functions

    // Kill move tutorial in the game
    killTutorial: function(){
      if(this.tutorial == null){
        return;
      }
      
      if(this.tutorial.kill()){
        this.tutorial = null;
        this.playerMovedOnce = true;
      }
    },

    /**
     * Add player score
     * @param {int} value 
     */
    addScore: function( value ){
      if(!this.isGameOver){
        this.score += value;
      }
    },

    /**
     * Make sure there is enemy settings to spawn.
     * Also control how strong the enemies spawned by controlling the wave number.
     */
    updateEnemies: function(){
      if(this.levelManager.enemiesSet.length == this.levelManager.currentPointer){
        if(this.levelManager.currentPointer != 0){
          this.wave++;
        }
        this.levelManager.setEnemies();
      }

      // console.log("Wave: " + this.wave +
      //   "\nPointer: " + this.levelManager.currentPointer);
    },

    /**
     * Validation rule whether can spawn new enemy or not.
     */
    canSpawnEnemy: function(){
      // Only spawn enemy after player finished move tutorial
      if(!this.playerMovedOnce){
        return false;
      }
      if(this.arrayEnemyInStage.length > this.maxEnemySpawned){
        return false;
      }

      if(this.player.isBroken){
        return false;
      }

      return true;
    },

    /**
     * Called when enemy is spawned.
     * Called from EntityEnemy init() function
     * @param {EntityEnemy} enemy 
     */
    enemySpawned: function ( enemy ){
      this.arrayEnemyInStage.push(enemy);
    },

    /**
     * Called when enemy is dead.
     * Called from EntityEnemy kill() function
     * @param {EntityEnemy} enemy 
     */
    enemyDead: function( enemy ){
      if(enemy instanceof EntityEnemyGreen){
        this.addScore(1);
      } 
      
      else if(enemy instanceof EntityEnemyOrange){
        this.addScore(2);
      }
      
      else if(enemy instanceof EntityEnemyRed){
        this.addScore(3);
      }

      this.arrayEnemyInStage.erase(enemy)
    },

    /**
     * What happens when player got hit.
     * Called from player receiveDamage function
     */
    playerBroken: function(){

      if (this.wave <= 2) {
        this.wave = 1;
      }
      else if (this.wave < 7) {
        this.wave -= 2;
      }else{
        this.wave -= 3;
      }

      this.levelManager.currentPointer = 0;
      this.levelManager.enemiesSet.length = 0;

      this.bg1.pause();
      this.bg2.pause();

      // Gives some time before the drone appears again
      this.timerSpawnDrone.reset();

      console.log("Player broken. \nReset to wave " + this.wave + "\nPointer " + this.levelManager.currentPointer);
    },

    /**
     * What happens when player fixed
     * Called from playerFixed function in player.js
     */
    playerFixed: function(){
      this.bg1.unpause();
      this.bg2.unpause();
    },

    // Pause Menu
    openSettings: function(){
      this.popUpSettings.easeIn();
      this.btnPause.disableClick();
      this.isGamePause = true;

      // Special to game background
      this.bg1.pause();
      this.bg2.pause();
    },

    closeSettings: function(){
      this.popUpSettings.easeOut();
      this.btnPause.enableClick();
      this.isGamePause = false;

      // Special to game background
      this.bg1.unpause();
      this.bg2.unpause();
    },

    gameOver: function(){
      this.btnPause.disableClick();
      // this.isGamePause = true;
      this.isGameOver = true;

      this.setHighscore(this.score);

      // API_END_GAME
      // Score: this.score

      this.bg1.pause();
      this.bg2.pause();

      ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
        .setTimeOut(2.4, function () {
          this.openGameoverPanel();
          ig.Timer.timeScale = 1;
        }.bind(this));
    },

    // Gameover Panel
    openGameoverPanel: function(){
      this.popUpGameover.easeIn();
    },

    // Scoring system
    getHighscore: function(){
      var highscore = ig.game.load("sh-highscore");
      if(highscore == null){
        highscore = 0;
      }
      return highscore;
    },

    setHighscore: function( score ){
      var highscore = this.getHighscore();
      if(score > highscore){
        ig.game.save("sh-highscore", score);
      }
    }
  });
});
