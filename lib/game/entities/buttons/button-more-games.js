ig.module('game.entities.buttons.button-more-games')
	.requires(
		'game.entities.buttons.button'
		, 'plugins.clickable-div-layer'
	)
	.defines(function () {
		EntityButtonMoreGames = EntityButton.extend({
			type: ig.Entity.TYPE.A,
			gravityFactor: 0,
			logo: new ig.AnimationSheet('media/graphics/sprites/menu/button-more-games.png', 184, 184),
			size: {
				x: 184,
				y: 184,
			},
			zIndex: 0,
			clickableLayer: null,
			link: null,
			newWindow: false,
			div_layer_name: "more-games",
			name: "moregames",

			isTweening: false,

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				//ig.soundHandler.unmuteAll(true);

				if (ig.global.wm) {
					return;
				}

				if (settings.div_layer_name) {
					//console.log('settings found ... using that div layer name')
					this.div_layer_name = settings.div_layer_name;
				}
				else {
					this.div_layer_name = 'more-games'
				}

				this.scaling = true;
			},
			setButtonLink: function(){
				if (_SETTINGS.MoreGames.Enabled) {
					this.anims.idle = new ig.Animation(this.logo, 0, [0], true);
					this.currentAnim = this.anims.idle;

					if (_SETTINGS.MoreGames.Link) {
						this.link = _SETTINGS.MoreGames.Link;
					}
					if (_SETTINGS.MoreGames.NewWindow) {
						this.newWindow = _SETTINGS.MoreGames.NewWindow;
					}
					this.clickableLayer = new ClickableDivLayer(this);
				}
				else {
					this.kill();
				}
			},

			update: function () {
				this.parent();

				// Desktop Mode, detect mouse hover and animate button
				var mouseLoc = ig.game.io.mouse.getPos();
				if (!ig.ua.mobile &&
					mouseLoc.x >= this.pos.x && mouseLoc.x <= this.pos.x + this.size.x
					&& mouseLoc.y >= this.pos.y && mouseLoc.y <= this.pos.y + this.size.y) {
					this.mouseOver = true;
					// this.setScale(1.10, 1.10);
				} else {
					this.mouseOver = false;

					if(!this.isTweening){
						// this.setScale(1, 1);
					}
				}
			},

			show: function (seconds, callback) {
				// var elem = ig.domHandler.getElementById("#" + this.div_layer_name);
				// if (elem) { ig.domHandler.show(elem); }

				// Tweening
				this.isTweening = true;
				if (seconds == null) seconds = 0.5;

				// Go back to real position
				this.pos.x = this.originalPos.x;
				this.pos.y = this.originalPos.y;
				this.bodyScale = 0;

				this.tween({ bodyScale: 1 }, seconds, {
					easing: ig.Tween.Easing.Back.EaseOut,
					onComplete: function () {
						this.disable = false;
						this.isTweening = false;

						callback ? callback() : "";
					}.bind(this)
				}).start();
			},
			hide: function (seconds, callback) {
				// var elem = ig.domHandler.getElementById("#" + this.div_layer_name);
				// if (elem) { ig.domHandler.hide(elem); }

				// Tweening
				this.isTweening = true;
				if (seconds == null) seconds = 0.5;

				this.tween({ bodyScale: 0 }, seconds, {
					easing: ig.Tween.Easing.Back.EaseIn,
					onComplete: function () {
						this.disable = false;
						this.isTweening = false;

						callback ? callback() : "";

						// Go out screen
						this.pos.y = -1000;
					}.bind(this)
				}).start();
			},
			clicked: function () {
				this.parent();
			},
			clicking: function () {

			},
			released: function () {

			},

			disableClick: function () {
				this.parent();
				var elem = ig.domHandler.getElementById("#" + this.div_layer_name);
				if (elem) { ig.domHandler.hide(elem); }
			},

			enableClick: function () {
				this.parent();
				var elem = ig.domHandler.getElementById("#" + this.div_layer_name);
				if (elem) { ig.domHandler.show(elem); }
			},
		});
	});