ig.module('game.entities.buttons.button')
	.requires(
		'game.entities.others.marketjs-entity',
		'plugins.data.vector'
	)
	.defines(function () {
		EntityButton = EntityMarketjsEntity.extend({
			zIndex: 0,
			collides: ig.Entity.COLLIDES.NEVER,
			type: ig.Entity.TYPE.A,
			fillColor: null,
			zIndex: 95000,
			originalPos: null,
			disable: false,
			isClicking: false,
			isTweening: false,

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				if (!ig.global.wm) {
					if (!isNaN(settings.zIndex)) {
						this.zIndex = settings.zIndex;
					}
				}
				//Pick a random color
				var r = Math.floor(Math.random() * 256);
				var g = Math.floor(Math.random() * 256);
				var b = Math.floor(Math.random() * 256);
				var a = 1;
				this.fillColor = "rgba(" + r + "," + b + "," + g + "," + a + ")";

				this.scaling = true;
			},
			clicked: function () {
				if(!this.disable){
					// Play sfx
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.buttonClick);
				}
			},
			clicking: function () {
				throw "no implementation on clicking()";
			},
			released: function () {
				throw "no implementation on released()";
			},

			disableClick: function () {
				this.disable = true;
			},

			enableClick: function () {
				this.disable = false;
			},

			show: function (seconds, callback) {
				this.disable = true;
				this.isTweening = true;
				if (seconds == null) seconds = 0.5;

				// Go back to real position
				this.pos.x = this.originalPos.x;
				this.pos.y = this.originalPos.y;
				this.bodyScale = 0;

				this.tween({ bodyScale: 1 }, seconds, {
					easing: ig.Tween.Easing.Back.EaseOut,
					onComplete: function () {
						this.disable = false;
						this.isTweening = false;

						callback ? callback() : "";
					}.bind(this)
				}).start();
			},

			hide: function (seconds, callback) {
				this.disable = true;
				this.isTweening = true;

				if (seconds == null) seconds = 0.5;

				this.tween({ bodyScale: 0 }, seconds, {
					easing: ig.Tween.Easing.Back.EaseIn,
					onComplete: function () {
						this.disable = false;
						this.isTweening = false;

						callback ? callback() : "";

						// Go out screen
						this.pos.y = -1000;
					}.bind(this)
				}).start();
			},

			setMiddlePosition: function (posX, posY) {
				this.pos.x = posX - this.size.x / 2;
				this.pos.y = posY - this.size.y / 2;

				this.originalPos = new Vector2(this.pos.x, this.pos.y);
			}
		});
	});