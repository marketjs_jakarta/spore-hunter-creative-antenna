ig.module('game.entities.buttons.button-move')
	.requires(
		'impact.entity',
		'game.entities.buttons.button'
	)
	.defines(function () {
		/**
		 * Invisible button to trigger player movement
		 */
		EntityButtonMove = EntityButton.extend({
			zIndex: 35,
			gravityFactor: 0,

			size: {x: 540, y: 960},

			init: function (x, y, settings) {
				this.parent(x, y, settings);
			},

			clicked: function () {
				if(this.disable) return;
				if(this.isClicking) return;

				this.controller.player.requestToMove();
			},

			clicking: function () {
				
			},

			released: function () {
				this.controller.player.requestToStop();
			}
		});
	});
