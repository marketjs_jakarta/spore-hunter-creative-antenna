ig.module('game.entities.buttons.button-restart')
	.requires(
		'impact.entity',
		'game.entities.buttons.button'
	)
	.defines(function () {
		EntityButtonRestart = EntityButton.extend({
			gravityFactor: 0,
			// marketjs entity properties
			idleSheetInfo: { sheetImage:new ig.Image('media/graphics/sprites/game/button-restart.png'), sheetX:1, sheetY:1 },
			scaling: true,
			// other entity properties
			offsetY: 0,
			pointerFocus: false,
	
			init: function( x, y, settings ) {
				
				this.parent( x, y, settings );
	
				// create animation
				this.idle = new ig.Animation(this.idleSheet, 1, [0]);
				this.currentAnim = this.idle;
						
			},
	
			update: function() {
				this.parent();
	
				this.pointerCollide();
			},
	
			pointerCollide: function() {
	
				if(ig.ua.mobile) return;
				if(ig.game.easing) return;
	
				var pointer = ig.game.getEntitiesByType(EntityPointerExtended)[0];
				
				if(	this.pointerFocus &&
					this.pos.x <= pointer.pos.x &&
					this.pos.y <= pointer.pos.y &&
					this.pos.x+this.size.x >= pointer.pos.x &&
					this.pos.y+this.size.y+5 >= pointer.pos.y
					) {
					return;
				}
	
				if(	this.pos.x <= pointer.pos.x &&
					this.pos.y <= pointer.pos.y &&
					this.pos.x+this.size.x >= pointer.pos.x &&
					this.pos.y+this.size.y >= pointer.pos.y
					) {
					if(!this.pointerFocus) {
						this.offsetY = -5;
						this.pointerFocus = true;
					}
				} else {
					if(this.pointerFocus) {
						this.offsetY = 0;
						this.pointerFocus = false;
					}
				}
	
			},
	
			clicked:function() {
				this.parent();
	
				if(ig.game.easing == false) {
					// play sfx
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.buttonClick);
					ig.game.easing = true; // false on menu-title.js => easeIn()
					this.tween({bodyScale: 0.8}, 0.1, {
						easing: ig.Tween.Easing.Linear.EaseNone,
						onComplete:function(){
							this.tween({bodyScale: 1}, 0.1, {
								easing: ig.Tween.Easing.Linear.EaseNone,
								onComplete:function(){
									// Go to the game
									ig.game.gotoGame();
								}.bind(this)
							}).start();
						}.bind(this)
					}).start();
				}
	
			},
	
			clicking:function() {
				
			},
	
			released:function() {
			
			}
			
		});
	})
