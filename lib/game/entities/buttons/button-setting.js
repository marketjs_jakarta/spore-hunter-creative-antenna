ig.module('game.entities.buttons.button-setting')
	.requires(
		'impact.entity',
		'game.entities.buttons.button'
	)
	.defines(function () {
		EntityButtonSetting = EntityButton.extend({
			zIndex: 0,

			// marketjs entity properties
			idleSheetInfo: { sheetImage:new ig.Image('media/graphics/sprites/menu/button-settings.png'), sheetX:1, sheetY:1 },

			init: function (x, y, settings) {
				this.parent(x, y, settings);
				this.idle = new ig.Animation(this.idleSheet, 1, [0]);
				this.currentAnim = this.idle;
			},

			ready: function () { },

			clicked: function () {
				this.parent();

				if(this.disable) return;
				if(this.isClicking) return;

				if (this.isClicking == false) {
					this.isClicking = true;
					this.tween({ bodyScale: 0.8 }, 0.1, {
						onComplete: function () {
							this.tween({ bodyScale: 1 }, 0.1, {
								onComplete: function () {
									// Action of click
									this.controller.openSettings();
									//////////
									this.isClicking = false;
								}.bind(this)
							}).start();
						}.bind(this)
					}).start();
				}
			},

			clicking: function () {
				
			},

			released: function () {
				
			}
		})
	})
