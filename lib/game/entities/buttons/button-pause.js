ig.module('game.entities.buttons.button-pause')
	.requires(
		'impact.entity',
		'game.entities.buttons.button'
	)
	.defines(function () {
		EntityButtonPause = EntityButton.extend({
			zIndex: 40,
			gravityFactor: 0,
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/button-pause.png'), sheetX: 1, sheetY: 1 },

			init: function (x, y, settings) {
				this.parent(x, y, settings);
				this.idle = new ig.Animation(this.idleSheet, 1, [0]);
				this.currentAnim = this.idle;

				this.originalPos = new Vector2(this.pos.x, this.pos.y);
			},

			ready: function () { },

			clicked: function () {
				this.parent();

				if(this.disable) return;
				if(this.isClicking) return;

				if (this.isClicking == false) {
					this.isClicking = true;
					this.tween({ bodyScale: 0.8 }, 0.1, {
						onComplete: function () {
							this.tween({ bodyScale: 1 }, 0.1, {
								onComplete: function () {
									// Open setting panel
									this.controller.openSettings();
									
									this.isClicking = false;
								}.bind(this)
							}).start();
						}.bind(this)
					}).start();
				}
			},

			clicking: function () {
				
			},

			released: function () {
				
			}
		})
	})
