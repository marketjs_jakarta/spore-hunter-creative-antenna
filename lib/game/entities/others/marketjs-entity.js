ig.module('game.entities.others.marketjs-entity')
  .requires(
    'impact.entity',
    'plugins.data.vector'
  )
  .defines(function () {
    EntityMarketjsEntity = ig.Entity.extend({
      // define image for animation
      idleSheetInfo: null,

      // for scalinng => add 'plugins.scale' in main.js
      scaling: false,
      bodyScale: 1,

      init: function (x, y, settings) {
        // for animation
        if (this.idleSheetInfo != null) {
          this.setSpriteSheet("idle");
          this.setSize("idle");
        }
        this.parent(x, y, settings);
      },
      setSpriteSheet: function (state) {
        this[state + "Sheet"] = new ig.AnimationSheet(this[state + "SheetInfo"].sheetImage.path,
          this[state + "SheetInfo"].sheetImage.width / this[state + "SheetInfo"].sheetX,
          this[state + "SheetInfo"].sheetImage.height / this[state + "SheetInfo"].sheetY);
      },
      setSize: function (state) {
        this.size.x = this[state + "SheetInfo"].sheetImage.width / this[state + "SheetInfo"].sheetX;
        this.size.y = this[state + "SheetInfo"].sheetImage.height / this[state + "SheetInfo"].sheetY;
      },

      update: function() {
			
        this.parent();
  
        // for scaling size	
        if(this.scaling == true) {
          this.setScale(this.bodyScale, this.bodyScale);
        }
            
      },

      setTopMiddlePosition: function( posX, posY ){
        this.pos.x = posX - this.size.x / 2;
        this.pos.y = posY;
      },

      getTopMiddlePosition: function(  ){
        var pos = new Vector2();
        pos.x = this.pos.x + this.size.x / 2;
        pos.y = this.pos.y;
        return pos;
      },

      setMiddlePosition: function( posX, posY ){
        this.pos.x = posX - this.size.x / 2;
        this.pos.y = posY - this.size.y / 2;
      },

      getMiddlePosition: function(){
        var pos = new Vector2();
        pos.x = this.pos.x + this.size.x / 2;
        pos.y = this.pos.y + this.size.y / 2;
        return pos;
      },

      setBottomMiddlePosition: function( posX, posY ){
        this.pos.x = posX - this.size.x / 2;
        this.pos.y = posY - this.size.y;
      },

      getBottomMiddlePosition: function(){
        var pos = new Vector2();
        pos.x = this.pos.x + this.size.x / 2;
        pos.y = this.pos.y + this.size.y;
        return pos;
      },

      // toString Function
      stringPosition: function(){
        var str = "pos: {" + this.pos.x + ", " + this.pos.y + "}";
        return str;
      },

      stringSize: function(){
        var str = "size: {" + this.size.x + ", " + this.size.y + "}";
        return str;
      }
    });
  });
