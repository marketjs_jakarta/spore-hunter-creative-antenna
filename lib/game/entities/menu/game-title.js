ig.module('game.entities.menu.game-title')
	.requires(
		'game.entities.others.marketjs-entity'
	)
	.defines(function () {
		EntityGameTitle = EntityMarketjsEntity.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/menu/title.png'), sheetX: 1, sheetY: 1 },

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create aimation
                this.idle = new ig.Animation(this.idleSheet, 1, [0]);
                this.currentAnim = this.idle;
            }
		});
	});