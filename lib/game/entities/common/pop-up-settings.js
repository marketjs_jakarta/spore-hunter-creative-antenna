ig.module('game.entities.common.pop-up-settings')

.requires(	
	'game.entities.others.marketjs-entity',
	'game.entities.buttons.button',
	'game.entities.buttons.button-home'
)

.defines(function() {

	EntityPopUpSettings = EntityMarketjsEntity.extend({
		// impact entity properties
		gravityFactor: 0,
		zIndex: 50,
		// marketjs entity propertis
		idleSheetInfo: { sheetImage:new ig.Image('media/graphics/sprites/common/settings-window.png'), sheetX:1, sheetY:1 },
		// other entity properties
		titleText: new ig.Image('media/graphics/sprites/common/settings-text.png'),
		titleTextPause: new ig.Image('media/graphics/sprites/common/paused-text.png'),
		iconBgm: new ig.Image('media/graphics/sprites/common/music-icon.png'),
		iconSfx: new ig.Image('media/graphics/sprites/common/sound-icon.png'),
		buttonClose: null,
		buttonHome: null,
		barBgm: null,
		barSfx: null,

		init: function( x, y, settings ) {
			
			this.parent( x, y, settings );

			// create animation
			this.idle = new ig.Animation(this.idleSheet, 1, [0]);
			this.currentAnim = this.idle;

			// set position horizontally and vertically
			this.setMiddlePosition(settings.controller.rWidthH, settings.controller.rHeightH);
			
			// set position before first tween
			this.pos.y += ig.system.height;

			// create entities
			this.blackBg = ig.game.spawnEntity(EntityBlackBackground, 0, 0, {controller: this.controller, zIndex:this.zIndex-1});
			this.barBgm = ig.game.spawnEntity(EntityPUSettingsBarBgm, ig.system.width, ig.system.height, {controller: this.controller, zIndex:this.zIndex+1});
			this.barSfx = ig.game.spawnEntity(EntityPUSettingsBarSfx, ig.system.width, ig.system.height, {controller: this.controller, zIndex:this.zIndex+1});
			if(this.controller instanceof EntityMenuController){
				this.buttonClose = ig.game.spawnEntity(EntityPUSettingsButtonClose, ig.system.width, ig.system.height, {controller: this.controller, zIndex:this.zIndex+1});
			}else{
				this.buttonClose = ig.game.spawnEntity(EntityPUSettingsButtonClose, ig.system.width, ig.system.height, {controller: this.controller, zIndex:this.zIndex+1});
				this.buttonHome = ig.game.spawnEntity(EntityButtonHome, ig.system.width, ig.system.height, {controller: this.controller, zIndex:this.zIndex+1});
			}
		},

		update: function() {

			this.parent();
			
			// set position entities
			this.barBgm.pos.x = this.pos.x+this.size.x*8.8/10 - this.barBgm.size.x;
			this.barBgm.pos.y = this.pos.y+this.size.y*9/20 - this.barBgm.size.y*1/2;
			this.barSfx.pos.x = this.pos.x+this.size.x*8.8/10 - this.barSfx.size.x;
			this.barSfx.pos.y = this.pos.y+this.size.y*14/20 - this.barSfx.size.y*1/2;
			
			if(this.controller instanceof EntityMenuController){
				this.buttonClose.pos.x = this.pos.x+this.size.x*1/2 - this.buttonClose.size.x*1/2;
				this.buttonClose.pos.y = this.pos.y+this.size.y*26/20 - this.buttonClose.size.y*1/2 + this.buttonClose.offsetY;
			}else{
				this.buttonHome.pos.x = this.pos.x+this.size.x*1/4 - this.buttonHome.size.x*1/2;
				this.buttonHome.pos.y = this.pos.y+this.size.y*26/20 - this.buttonHome.size.y*1/2 + this.buttonHome.offsetY;
				this.buttonClose.pos.x = this.pos.x+this.size.x*3/4 - this.buttonClose.size.x*1/2;
				this.buttonClose.pos.y = this.pos.y+this.size.y*26/20 - this.buttonClose.size.y*1/2 + this.buttonClose.offsetY;
			}
		},

		draw: function() {

			this.parent();

			ig.system.context.save();

			// title
			if(this.controller instanceof EntityMenuController){
				var posTitle = {x: this.pos.x+this.size.x*1/2-this.titleText.width/2, y: this.pos.y+this.size.y*2.6/20};
				this.titleText.draw(posTitle.x, posTitle.y);
			}else{
				var posTitle = {x: this.pos.x+this.size.x*1/2-this.titleTextPause.width/2, y: this.pos.y+this.size.y*2.6/20};
				this.titleTextPause.draw(posTitle.x, posTitle.y);
			}

            ig.system.context.restore();
		},

		easeOut: function() {
			this.blackBg.easeOut();
			this.tween({pos:{y: this.pos.y-ig.system.height}}, 0.2, {
				easing: ig.Tween.Easing.Linear.EaseNone,
				onComplete:function(){
					this.pos.y += ig.system.height*2;
				}.bind(this)
			}).start();
		},

		easeIn: function() {
			this.blackBg.easeIn();
			this.tween({pos:{y: this.pos.y-ig.system.height}}, 0.2, {
				easing: ig.Tween.Easing.Linear.EaseNone,
				onComplete:function(){
					ig.game.easing = false;
				}.bind(this)
			}).start();
		}
	});
	
	EntityPUSettingsButtonClose = EntityButton.extend({
		gravityFactor: 0,
		// marketjs entity properties
		idleSheetInfo: { sheetImage:new ig.Image('media/graphics/sprites/common/button-back.png'), sheetX:1, sheetY:1 },
		scaling: true,
		// other entity properties
		offsetY: 0,
		pointerFocus: false,

		init: function( x, y, settings ) {
			
			this.parent( x, y, settings );

			// create animation
			this.idle = new ig.Animation(this.idleSheet, 1, [0]);
			this.currentAnim = this.idle;
					
		},

		update: function() {

			this.parent();

			this.pointerCollide();

		},

		pointerCollide: function() {

			if(ig.ua.mobile) return;
			if(ig.game.easing) return;

			var pointer = ig.game.getEntitiesByType(EntityPointerExtended)[0];
			
			if(	this.pointerFocus &&
				this.pos.x <= pointer.pos.x &&
				this.pos.y <= pointer.pos.y &&
				this.pos.x+this.size.x >= pointer.pos.x &&
				this.pos.y+this.size.y+5 >= pointer.pos.y
				) {
				return;
			}

			if(	this.pos.x <= pointer.pos.x &&
				this.pos.y <= pointer.pos.y &&
				this.pos.x+this.size.x >= pointer.pos.x &&
				this.pos.y+this.size.y >= pointer.pos.y
				) {
				if(!this.pointerFocus) {
					this.offsetY = -5;
					this.pointerFocus = true;
				}
			} else {
				if(this.pointerFocus) {
					this.offsetY = 0;
					this.pointerFocus = false;
				}
			}

		},

		clicked:function() {
			this.parent();

			if(ig.game.easing == false) {
				ig.game.easing = true; // false on menu-title.js => easeIn()
				this.tween({bodyScale: 0.8}, 0.1, {
					easing: ig.Tween.Easing.Linear.EaseNone,
					onComplete:function(){
						this.tween({bodyScale: 1}, 0.1, {
							easing: ig.Tween.Easing.Linear.EaseNone,
							onComplete:function(){
								this.controller.closeSettings();
							}.bind(this)
						}).start();
					}.bind(this)
				}).start();
			}

		},

		clicking:function() {
			
		},

		released:function() {
		
		}
		
	});

	EntityPUSettingsBar = EntityMarketjsEntity.extend({
		gravityFactor: 0,
		// marketjs entity properties
		idleSheetInfo: { sheetImage:new ig.Image('media/graphics/sprites/common/bar-1.png'), sheetX:1, sheetY:1 },
		// other entity properties
		bar: new ig.Image('media/graphics/sprites/common/bar-2.png'),
		volume: 1, // 0 - 1

		init: function( x, y, settings ) {
			this.parent( x, y, settings );

			// create animation
			this.idle = new ig.Animation(this.idleSheet, 1, [0], true);
			this.currentAnim = this.idle;

			this.sliderBar = ig.game.spawnEntity(EntityPUSettingsSliderBar, ig.system.width, ig.system.height, {bar: this, zIndex: this.zIndex+1});
		},

		update: function() {

			this.parent();	

			if(this.move) {
				// single click

				// get data from cursor
				var cursorX = ig.game.io.getClickPos().x - this.pos.x;
				if (cursorX < 0) {
					cursorX = 0;
				} else if (cursorX > this.size.x) {
					cursorX = this.size.x;
				}
				
				this.volume = cursorX / this.size.x;
				
				// set after click

				if(ig.input.released("click")) {
					this.sliderBar.released();
				}
			}

			// this.sliderBar.pos.x = this.pos.x+this.sliderBar.size.x*1/2 + (this.bar.width-this.sliderBar.size.x)*this.bodyScale*this.volume - this.sliderBar.size.x*1/2;
			// this.sliderBar.pos.y = this.pos.y+this.size.y*1/2 - this.sliderBar.size.y*1/2;

			this.sliderBar.setMiddlePosition(this.pos.x + this.volume * this.size.x, this.pos.y + this.size.y / 2);
		},

		draw: function() {

			this.parent();

			if( this.volume > 0 ) {				
				ig.system.context.save();
				var imgWidth = this.volume > 100 ? 100 : this.volume;
				ig.system.context.drawImage(
					this.bar.data,
					0,
					0,
					this.bar.width*imgWidth,
					this.bar.height,
					this.pos.x+this.size.x*1/2 - this.bar.width*this.bodyScale/2,
					this.pos.y+this.size.y*1/2 - this.bar.height*this.bodyScale/2,
					this.bar.width*this.bodyScale*imgWidth,
					this.bar.height*this.bodyScale
				);
	            ig.system.context.restore();
			}

		},
	
		updateTarget: function() {
			// update volume game and local storage here
		}
		
	});

	EntityPUSettingsSliderBar = EntityButton.extend({
		gravityFactor: 0,
		// marketjs entity properties
		idleSheetInfo: { sheetImage:new ig.Image('media/graphics/sprites/common/bar-3.png'), sheetX:1, sheetY:1 },

		init: function( x, y, settings ) {
			
			this.parent( x, y, settings );

			// create animation
			this.idle = new ig.Animation(this.idleSheet, 1, [0], true);
			this.currentAnim = this.idle;
					
		},

		clicked:function() {

			if(ig.game.easing == false) {
				// play sfx
				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.buttonClick);
				ig.game.easing = true;
				this.bar.move = true;
			}

		},

		clicking:function() {
			
		},

		released:function() {
			this.bar.updateTarget();
			this.bar.move = false;
			ig.game.easing = false;
			// play sfx
			ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.buttonClick);
		}
		
	});

	EntityPUSettingsBarBgm = EntityPUSettingsBar.extend({

		// other entity properties
		ico: new ig.Image('media/graphics/sprites/common/music-icon.png'),

		init: function( x, y, settings ) {
			
			this.parent( x, y, settings );

			if(ig.game.volumeBgm != null){
				this.volume = ig.game.volumeBgm;
			}
		},

		update: function() {

			this.parent();
			
			if(this.move) {
				// update volume game
				ig.game.volumeBgm = this.volume;

	            // Set volume
	            ig.soundHandler.bgmPlayer.volume(ig.game.volumeBgm);
	        }
		
		},

		draw: function() {

			this.parent();

			ig.system.context.save();
			ig.system.context.drawImage(
				this.ico.data,
				this.controller.popUpSettings.pos.x+this.controller.popUpSettings.size.x*0.5/10,
				this.pos.y+this.size.y*1/2 - this.ico.height*this.bodyScale/2,
				this.ico.width*this.bodyScale,
				this.ico.height*this.bodyScale
			);
            ig.system.context.restore();

		},
	
		updateTarget: function() {
			// update local storage
			ig.game.save("sh-volumeBgm", this.volume);
		}
		
	});

	EntityPUSettingsBarSfx = EntityPUSettingsBar.extend({

		// other entity properties
		ico: new ig.Image('media/graphics/sprites/common/sound-icon.png'),

		init: function( x, y, settings ) {		
			
			this.parent( x, y, settings );

			if(ig.game.volumeSfx != null){
				this.volume = ig.game.volumeSfx;
			}
		},

		update: function() {

			this.parent();
			
			if(this.move) {
				// update volume game
				ig.game.volumeSfx = this.volume;

				// Set volume
	            ig.soundHandler.sfxPlayer.volume(ig.game.volumeSfx);
	        }			
		
		},

		draw: function() {

			this.parent();

			ig.system.context.save();
			ig.system.context.drawImage(
				this.ico.data,
				this.controller.popUpSettings.pos.x+this.controller.popUpSettings.size.x*0.5/10,
				this.pos.y+this.size.y*1/2 - this.ico.height*this.bodyScale/2,
				this.ico.width*this.bodyScale,
				this.ico.height*this.bodyScale
			);
            ig.system.context.restore();

		},
	
		updateTarget: function() {
			// update local storage
			ig.game.save("sh-volumeSfx", this.volume);
		}
		
	});

	EntityBlackBackground = EntityMarketjsEntity.extend({
		gravityFactor: 0,
		idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/common/black-50.png'), sheetX: 1, sheetY: 1 },
		// other entity properties
		blackBg: new ig.Image('media/graphics/sprites/common/black-50.png'),
		opacity: 0,

		init: function( x, y, settings ) {
			this.parent( x, y, settings );

			// create aimation
			this.idle = new ig.Animation(this.idleSheet, 1, [0]);
			this.currentAnim = this.idle;
		},

		update: function(){
			// Change Opacity
			this.currentAnim.alpha = this.opacity;
		},

		easeOut: function() {
			console.log("Bg ease out");
			this.opacity = 0;	// Delete this line when tweening is working
			// Fade out
			this.tween({ opacity: 0 }, 0.2, {
				easing: ig.Tween.Easing.Linear.EaseNone,
				onComplete:function(){
					console.log("Bg ease out complete");
					// Go out screen
					this.pos.x = ig.system.width;
					this.pos.y = ig.system.height;
				}.bind(this)
			}).start();
		},

		easeIn: function() {
			console.log("Bg ease in");
			this.opacity = 1;	// Delete this line when tweening is working
			this.tween({ opacity: 1 }, 0.2, {
				easing: ig.Tween.Easing.Linear.EaseNone,
				onComplete:function(){
					console.log("Bg ease in complete");
					// Back to screen
					this.pos.x = 0;
					this.pos.y = 0;
				}.bind(this)
			}).start();
		}
	});

});