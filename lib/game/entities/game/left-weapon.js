ig.module('game.entities.game.left-weapon')
	.requires(
		'game.entities.game.attached-weapon'
	)
	.defines(function () {
		EntityLeftWeapon = EntityAttachedWeapon.extend({
	            idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/left-weapon.png'), sheetX: 1, sheetY: 1 }
		});
	});