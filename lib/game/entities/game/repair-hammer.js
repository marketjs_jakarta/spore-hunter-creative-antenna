ig.module('game.entities.game.repair-hammer')
	.requires(
        'impact.entity-pool',

        'game.entities.others.marketjs-entity',
        'plugins.data.vector'
	)
	.defines(function () {
		EntityRepairHammer = EntityMarketjsEntity.extend({
            gravityFactor: 0,
            idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/hammer.png'), sheetX: 1, sheetY: 1 },
            rotation: 0,

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create animation
                this.idle = new ig.Animation(this.idleSheet, 1, [0]);
                this.currentAnim = this.idle;
            },

            reset: function (x, y, settings) {
				this.parent(x, y, settings);

            },
            
            update: function () {
				this.parent();

                this.currentAnim.angle = this.rotation;
            },

            tweenPull: function(){
                // Implement in subclass
            },

            tweenHit: function(){
                // Implement in subclass
            }
        });
        
	});