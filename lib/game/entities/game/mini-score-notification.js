ig.module('game.entities.game.mini-score-notification')
	.requires(
        'impact.entity-pool',

        'game.entities.others.marketjs-entity'
	)
	.defines(function () {
        /**
         * Only for visual. Don't affect any gameplay.
         * A small pop-up-like notification.
         */
		EntityMiniScoreNotification = EntityMarketjsEntity.extend({
            gravityFactor: 0,
            text: "TEXT",

			init: function (x, y, settings) {
                this.parent(x, y, settings);
                
                this.tween({ pos: {y: this.pos.y - 50} }, 0.8, {
                    easing: ig.Tween.Easing.Linear.EaseNone,
                    onComplete: function () {
                        this.kill();
                    }.bind(this)
                }).start();
            },

            draw: function(){
                this.parent();

                var ContextCanvas = ig.system.context;
                ContextCanvas.save();

                var fontSize = 20;
                var fontStyle = "porkys";

                ContextCanvas.font = "bold " + fontSize + "px " + fontStyle;
                ContextCanvas.fillStyle = "#ffffff";
                ContextCanvas.textAlign = 'center';
                ContextCanvas.textBaseline = "alphabetic";
                ContextCanvas.shadowOffsetX = 0;
                ContextCanvas.shadowOffsetY = 0;
                var shadowColor = "red";
                var shadowBlur = 7;
                ig.KvnUtility.drawGlowingText(ContextCanvas, this.text, this.pos.x , this.pos.y, shadowColor, shadowBlur);

                ContextCanvas.restore();
            }
        });
        
	});