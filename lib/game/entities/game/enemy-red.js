ig.module('game.entities.game.enemy-red')
	.requires(
		'impact.entity-pool',
		'plugins.scale',

		'game.entities.game.enemy',
		'game.entities.game.enemy-orange',
		'game.entities.game.enemy-explosion'
	)
	.defines(function () {
		EntityEnemyRed = EntityEnemy.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/red-spore.png'), sheetX: 8, sheetY: 5 },

			// Animation stuff
			healthLimit1: 40,
			healthLimit2: 80,
			healthLimit3: 120
		});
	});