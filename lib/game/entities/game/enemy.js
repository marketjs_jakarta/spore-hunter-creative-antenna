ig.module('game.entities.game.enemy')
	.requires(
		'impact.entity-pool',
		'plugins.kvn-scale',	// don't use it together with plugins.scale
		'plugins.kvn-utility',

		'game.entities.others.marketjs-entity',
		'game.entities.game.enemy-explosion'
	)
	.defines(function () {
		EntityEnemy = EntityMarketjsEntity.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/green-spore.png'), sheetX: 8, sheetY: 5 },
			gravityFactor: 1,
			zIndex: 15,
			type: ig.Entity.TYPE.B,	// Enemy group
			checkAgainst: ig.Entity.TYPE.A,
			maxHealth: null,
			health: 10,
			maxVel: { x: 800, y: 800 },
			currentEnemyVariation: null,
			isTweening: false,
			savedVelocity: 0,	// Velocity before damaged and slowed down
			reboundSpeed: 620,
			topMarginPosition: 250,	// Above this position, bullets can't push it up
			isGoingDown: null,	// If false, its going up
			
			timerPushedBackCooldown: null,	// Prevent pushed back to fast, especially if shot by 3 bullets
			pushedBackCooldown: 0.01,		// Cooldown interval (shoot every x seconds)

			// Animation stuff
			healthLimit1: 15,		// Need to override on subclass
			healthLimit2: 30,		// Need to override on subclass
			healthLimit3: 45,		// Need to override on subclass

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create animation
				this.idle = new ig.Animation(this.idleSheet, 1, [33]);	// Idle in last color form (weakest)
				this.idle1 = new ig.Animation(this.idleSheet, 1, [32]);	// Second weakest
				this.idle2 = new ig.Animation(this.idleSheet, 1, [31]);	// Strong
				this.idle3 = new ig.Animation(this.idleSheet, 1, [30]);	// Strongest
				this.transition = new ig.Animation(this.idleSheet, 0.05, [20,21,22,23,24,25,26,27,28,29]);	// Transition to weakest
				this.transition1 = new ig.Animation(this.idleSheet, 0.05, [10,11,12,13,14,15,16,17,18,19]);	// Transition from strong
				this.transition2 = new ig.Animation(this.idleSheet, 0.05, [0,1,2,3,4,5,6,7,8,9]);	// Transition from strongest
				this.initAnim();

				// Make the collider fit to the image
				this.offset.x = 0.3 * this.size.x;
				this.offset.y = 0.36 * this.size.y;

				this.size.x = this.size.x / 3;
				this.size.y = this.size.x;

				// Make it center in the spawn position
				this.pos.x = this.pos.x - this.size.x / 2;

				if(settings.variation == null){
					// Set default if not defined
					this.currentEnemyVariation = settings.enemyVariation.medium;
				} else{
					this.currentEnemyVariation = settings.variation;
				}

				this.maxHealth = this.health;

				this.scaleInit();
				this.setScale(this.currentEnemyVariation.scale, this.currentEnemyVariation.scale);
				this.scaling = true;
				this.bodyScale = this.currentEnemyVariation.scale;
				this.isGoingDown = true;
				this.controller.enemySpawned(this);

				this.timerPushedBackCooldown = new ig.Timer();
			},

			reset: function (x, y, settings) {
				this.parent(x, y, settings);

				this.currentAnim = this.idle.rewind();

				this.pos.x = this.pos.x - this.size.x / 2;

				if(settings.variation == null){
					// Set default if not defined
					this.currentEnemyVariation = settings.enemyVariation.medium;
				} else{
					this.currentEnemyVariation = settings.variation;
				}

				this.setScale(this.currentEnemyVariation.scale, this.currentEnemyVariation.scale);
				this.bodyScale = this.currentEnemyVariation.scale;
			},

			update: function () {
				if(this.controller.isGamePause){
					return;
				}

				this.parent();

				// Set animation
				if(this.health < this.healthLimit1){
					if(this.currentAnim == this.idle1){
						this.currentAnim = this.transition.rewind();
					}else if(this.currentAnim == this.transition && this.currentAnim.frame == 9){
						this.currentAnim = this.idle;
					}
				}else if(this.health < this.healthLimit2){
					if(this.currentAnim == this.idle2){
						this.currentAnim = this.transition1.rewind();
					}else if(this.currentAnim == this.transition1 && this.currentAnim.frame == 9){
						this.currentAnim = this.idle1;
					}
				}else if(this.health < this.healthLimit3){
					if(this.currentAnim == this.idle3){
						this.currentAnim = this.transition2.rewind();
					}else if(this.currentAnim == this.transition2 && this.currentAnim.frame == 9){
						this.currentAnim = this.idle2;
					}
				}else {
					this.currentAnim = this.idle3;
				}

				// Control the speed
				var bottomLimitPos = this.controller.rHeight - this.size.y;
				var rightLimitPos = this.controller.rWidth - this.size.x;
				var leftLimitPos = 0;
				if (this.pos.y > bottomLimitPos) {
					this.vel.y = -this.reboundSpeed;
					this.isGoingDown = false;
				} else if (this.pos.x > rightLimitPos) {
					if (this.vel.x > 0) {
						this.vel.x = -this.vel.x;
					}
				} else if (this.pos.x < leftLimitPos) {
					if (this.vel.x < 0) {
						this.vel.x = -this.vel.x;
					}
				}

				// When is going up but the velocity already bigger than zero
				// then we know it's going down
				if(!this.isGoingDown){
					if(this.vel.y > 0){
						this.isGoingDown = true;
					}
				}
			},

			check: function ( other ){
				if(other instanceof EntityPlayer){
					other.receiveDamage( 1, this );
				}
			},

			draw: function(){
				this.parent();

				var ContextCanvas = ig.system.context;
				ContextCanvas.save();

				var	fontSize = this.currentEnemyVariation.fontSize;
				var fontStyle = "porkys";
				var text = "" + this.health;

				ContextCanvas.font = fontSize + "px " + fontStyle;
				ContextCanvas.fillStyle = "#ffffff";
				ContextCanvas.textAlign = 'center';
				ContextCanvas.textBaseline = "alphabetic";
				var strokeStyle = "#000000";
				var lineWidth = 1;
				
				var xPos = this.pos.x + this.size.x/2 - ig.game.screen.x;
				var yPos = this.pos.y + this.size.y/2 + fontSize / 2 - ig.game.screen.y;
				ig.KvnUtility.drawStrokedText(ContextCanvas, text, xPos, yPos, strokeStyle, lineWidth);

				ContextCanvas.restore();
			},

			receiveDamage: function( amount, from ){
				// Scale in out effect
				if(!this.isTweening){
					this.isTweening = true;
					this.tween({bodyScale: this.currentEnemyVariation.scale + 0.08}, 0.05, {
						easing: ig.Tween.Easing.Linear.EaseNone,
						onComplete:function() {
							this.tween({bodyScale: this.currentEnemyVariation.scale}, 0.05, {
								easing: ig.Tween.Easing.Linear.EaseNone,
								onComplete:function() {
									this.isTweening = false;
								}.bind(this)
							}).start();
						}.bind(this)
					}).start();
				}

				// Retard / shake effect to add more depth effect
				if(this.timerPushedBackCooldown.delta() > this.pushedBackCooldown){
					if(this.isGoingDown){
						// Push up a bit
						if(this.vel.y > 300){
							this.pos.y -= 10;
							if(this.vel.y > 400){
								this.vel.y -= 7;
							}
						}else{
							this.pos.y -= 5;
						}
					}else{
						// Push down little bit
						this.pos.y += 3;
					}
					this.timerPushedBackCooldown.reset();
				}

				this.health -= amount;
				if(this.health <= 0){
					this.kill();
				}
			},

			kill: function(){
				// Spawn enemy explosion
				var midPos = this.getMiddlePosition();
				ig.game.spawnEntity(EntityEnemyExplosion, midPos.x, midPos.y, {controller: this.controller});

				this.controller.enemyDead(this);
				// ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.enemyPop);
				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.smallExplosion);

				if(this.currentEnemyVariation.sizeIndex != 0){
					this.splitSelf();
				}

				this.parent();
			},

			/**
			 * Set first anim to use
			 */
			initAnim: function(){
				if(this.health < this.healthLimit1){
					this.currentAnim = this.idle;
				}else if(this.health < this.healthLimit2){
					this.currentAnim = this.idle1;
				}else if(this.health < this.healthLimit3){
					this.currentAnim = this.idle2;
				}else{
					this.currentAnim = this.idle3;
				}
			},

			/**
			 * Spawn 2 enemies that is smaller than this object.
			 */
			splitSelf: function(){
				var enemyHealth = ig.KvnUtility.getRandomInteger(Math.ceil(this.maxHealth * 0.4), Math.floor(this.maxHealth * 0.6));
				var currMiddlePos = this.getMiddlePosition();
				var newEnemyVariation = this.controller.enemyVariationArray[this.currentEnemyVariation.sizeIndex - 1];
				var originalSize = new Vector2(this.size.x / this.currentEnemyVariation.scale, this.size.y / this.currentEnemyVariation.scale);
				var smallerSize = new Vector2(originalSize.x * newEnemyVariation.scale, originalSize.y * newEnemyVariation.scale);
				var spawnPos = new Vector2(currMiddlePos.x, currMiddlePos.y);
				var enemyEntity = null;
				if(enemyHealth < 20){
					enemyEntity = EntityEnemyGreen;
				}else if(enemyHealth < 65){
					enemyEntity = EntityEnemyOrange;
				}else{
					enemyEntity = EntityEnemyRed;
				}
				var settingsLeft = {vel: {x: -60, y: -200}, health: enemyHealth, variation: newEnemyVariation, controller: this.controller};
				ig.game.spawnEntity(enemyEntity, spawnPos.x - 30, spawnPos.y, settingsLeft);
				
				var settingsRight = {vel: {x: 60, y: -200}, health: enemyHealth, variation: newEnemyVariation, controller: this.controller};
            	ig.game.spawnEntity(enemyEntity, spawnPos.x + 30, spawnPos.y, settingsRight);
			}
		});

		// ig.EntityPool.enableFor(EntityEnemy);
	});