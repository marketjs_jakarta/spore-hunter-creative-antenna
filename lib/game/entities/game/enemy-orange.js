ig.module('game.entities.game.enemy-orange')
	.requires(
		'impact.entity-pool',
		'plugins.scale',
		'plugins.kvn-utility',
		// 'plugins.data.vector',

		'game.entities.game.enemy',
		'game.entities.game.enemy-green',
		'game.entities.game.enemy-explosion'
	)
	.defines(function () {
		EntityEnemyOrange = EntityEnemy.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/orange-spore.png'), sheetX: 8, sheetY: 5 },

			// Animation stuff
			healthLimit1: 40,
			healthLimit2: 80,
			healthLimit3: 100
		});
	});