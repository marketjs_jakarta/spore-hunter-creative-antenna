ig.module('game.entities.game.bullet')
	.requires(
		'impact.entity-pool',

		'game.entities.others.marketjs-entity',
		'game.entities.game.enemy',
		'game.entities.game.enemy-special',
		'game.entities.game.loot-drop',
		'game.entities.game.trap-item'
	)
	.defines(function () {
		EntityBullet = EntityMarketjsEntity.extend({
			zIndex: 10,
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/shot.png'), sheetX: 1, sheetY: 1 },
			maxVel: { x: 1000, y: 1000 },
			gravityFactor: 0,
			checkAgainst: ig.Entity.TYPE.BOTH,

			// game logic properties
			isMoved: false,
			posClickOffsetX: 0,

			init: function (x, y, settings) {
				this.parent(x, y, settings);
				// create aimation
				this.idle = new ig.Animation(this.idleSheet, 1, [0]);
				this.currentAnim = this.idle;
				this.pos.x = this.pos.x - this.size.x / 2;
			},

			reset: function (x, y, settings) {
				this.parent(x, y, settings);

				this.pos.x = this.pos.x - this.size.x / 2;
			},

			update: function () {
				this.parent();
				if (this.pos.y < -0) {
					this.kill();
				}
				if(this.pos.x < 0){
					this.kill();
				}
				if(this.pos.x > this.controller.rWidth){
					this.kill();
				}
			},
			check: function(other){
				if(other instanceof EntityEnemy){
					other.receiveDamage(1, this);
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.laser1);
					this.controller.addScore(1);
					this.kill();
				}

				if(other instanceof EntityLootDrop){
					other.gotShot();
				}

				if(other instanceof EntityEnemySpecial){
					other.receiveDamage(1, this);
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.hitIron);
					this.kill();
				}
			}
		});

		// With pooling enabled, instances that are removed from the game world are not
		// completely erased, but rather put in a pool and resurrected when needed.

		ig.EntityPool.enableFor(EntityBullet);

	});