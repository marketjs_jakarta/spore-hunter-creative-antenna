ig.module('game.entities.game.upgrade-box')
	.requires(
        'game.entities.others.marketjs-entity',
        'game.entities.game.loot-drop'
	)
	.defines(function () {
		EntityUpgradeBox = EntityLootDrop.extend({
            	idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/upgrade-box.png'), sheetX: 1, sheetY: 1 }
		});
	});