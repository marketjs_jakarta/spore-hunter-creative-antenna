ig.module('game.entities.game.player-shield')
	.requires(
        'game.entities.game.attached-weapon'
	)
	.defines(function () {
		EntityPlayerShield = EntityAttachedWeapon.extend({
            	idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/shield.png'), sheetX: 1, sheetY: 1 }
		});
	});