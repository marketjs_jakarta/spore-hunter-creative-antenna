ig.module('game.entities.game.player-moves-vfx')
	.requires(
		'game.entities.game.attached-weapon'
	)
	.defines(function () {
		EntityPlayerMovesVfx = EntityAttachedWeapon.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/plane-moves.png'), sheetX: 2, sheetY: 2 },

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create animation
				this.idle = new ig.Animation(this.idleSheet, 0.1, [0, 1, 2, 3]);
				this.currentAnim = this.idle;
			},

			setTopMiddlePosition: function (posX, posY) {
				this.parent(posX, posY);

				// Reset offset
				this.offsetPos = new Vector2(this.parentObj.pos.x - this.pos.x, this.parentObj.pos.y - this.pos.y);
			}
		});
	});