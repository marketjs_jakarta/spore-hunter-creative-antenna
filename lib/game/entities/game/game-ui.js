ig.module('game.entities.game.game-ui')
        .requires(
                'plugins.kvn-utility',
                'game.entities.others.marketjs-entity',
                'game.entities.kvn-delay-execution'
        )
        .defines(function () {
                EntityGameUi = EntityMarketjsEntity.extend({
                        zIndex: 40,
                        gravityFactor: 0,

                        barFrameLife: new ig.Image('media/graphics/sprites/game/life-bar-frame.png'),
                        barFillLife: new ig.Image('media/graphics/sprites/game/life-bar-fill.png'),
                        shieldIcon: new ig.Image('media/graphics/sprites/game/shield-icon.png'),
                        lifeBarPosModifier: new Vector2(0, 0),

                        scoreScale: 1,
                        scorePosModifier: new Vector2(0, 0),

                        init: function (x, y, settings) {
                                this.parent(x, y, settings);

                                ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
                                        .setTimeOut(1, function () {
                                                this.show();
                                        }.bind(this));

                                this.lifeBarPosModifier.x = 280;
                                this.lifeBarPosModifier.y = 0;

                                this.scorePosModifier.x = 0;
                                this.scorePosModifier.y = -1000;
                        },

                        draw: function () {
                                this.parent();

                                var ContextCanvas = ig.system.context;

                                // Drawing life bar
                                ContextCanvas.save();

                                // if (this.controller.player.health > 0) {
                                //         var posX = this.controller.rWidth - this.barFrameLife.width - 10 + this.lifeBarPosModifier.x;
                                //         var posY = 10 + this.shieldIcon.height / 2 - this.barFrameLife.height / 2 + this.lifeBarPosModifier.y;
                                //         var fillWidth = this.barFrameLife.width * this.controller.player.health / 3;		// calculate width based on current healt per max health

                                //         // Draw the frame
                                //         this.barFrameLife.draw(posX, posY);

                                //         if (fillWidth > 0) {
                                //                 // Draw the fill
                                //                 this.barFillLife.draw(posX, posY, 0, 0, fillWidth, this.barFrameLife.height);
                                //         }

                                //         this.shieldIcon.draw(posX - this.shieldIcon.width, 10);
                                // }

                                var playerLife = this.controller.player.health;
                                var iconMargin = 5;
                                var iconWidth = this.shieldIcon.width;
                                var posX = this.controller.rWidth - iconWidth * playerLife - iconMargin * playerLife + this.lifeBarPosModifier.x;
                                var posY = 10;
                                
                                for (var i = 0; i < playerLife; i++) {
                                        this.shieldIcon.draw(posX, posY);
                                        posX = posX + iconWidth + iconMargin;
                                }

                                ContextCanvas.restore();

                                // Score UI
                                ContextCanvas.save();

                                var fontSize = 80 * this.scoreScale;
                                var fontStyle = "porkys";
                                var text = "" + this.controller.score;
                                var posY = fontSize + 100;

                                ContextCanvas.font = "bold " + fontSize + "px " + fontStyle;
                                ContextCanvas.fillStyle = "#ffffff";
                                ContextCanvas.textAlign = 'center';
                                ContextCanvas.textBaseline = "alphabetic";
                                ContextCanvas.shadowOffsetX = 0;
                                ContextCanvas.shadowOffsetY = 0;
                                var shadowColor = "red";
                                var shadowBlur = 10;
                                ig.KvnUtility.drawGlowingText(ContextCanvas, text, this.controller.rWidthH + this.scorePosModifier.x, posY + this.scorePosModifier.y, shadowColor, shadowBlur);

                                ContextCanvas.restore();
                        },

                        show: function (seconds, callback) {
                                this.showLifeBar();

                                ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
                                        .setTimeOut(0.3, function () {
                                                this.showScore();
                                        }.bind(this));
                        },

                        hide: function (seconds, callback) {
                                this.hideLifeBar();
                                this.hideScore();
                        },

                        showLifeBar: function (seconds, callback) {
                                if (seconds == null) seconds = 0.5;
                                this.lifeBarPosModifier.x = 280;
                                this.lifeBarPosModifier.y = 0;

                                this.tween({ lifeBarPosModifier: new Vector2(0, 0) }, seconds, {
                                        easing: ig.Tween.Easing.Back.EaseOut,
                                        onComplete: function () {
                                                callback ? callback() : "";
                                        }.bind(this)
                                }).start();
                        },

                        hideLifeBar: function (seconds, callback) {
                                if (seconds == null) seconds = 0.5;

                                this.tween({ lifeBarPosModifier: new Vector2(280, 0) }, seconds, {
                                        easing: ig.Tween.Easing.Back.EaseIn,
                                        onComplete: function () {
                                                callback ? callback() : "";
                                        }.bind(this)
                                }).start();
                        },

                        showScore: function (seconds, callback) {
                                if (seconds == null) seconds = 0.5;

                                // Go back to real position
                                this.scorePosModifier.x = 0;
                                this.scorePosModifier.y = 0;

                                // Set to pop in from scale 0
                                this.scoreScale = 0;

                                this.tween({ scoreScale: 1 }, seconds, {
                                        easing: ig.Tween.Easing.Back.EaseOut,
                                        onComplete: function () {
                                                callback ? callback() : "";
                                        }.bind(this)
                                }).start();
                        },

                        hideScore: function (seconds, callback) {
                                if (seconds == null) seconds = 0.5;

                                // Out from screen
                                this.scorePosModifier.x = 0;
                                this.scorePosModifier.y = -1000;

                                // Set to pop in from scale 0
                                this.scoreScale = 1;

                                this.tween({ scoreScale: 0 }, seconds, {
                                        easing: ig.Tween.Easing.Back.EaseOut,
                                        onComplete: function () {
                                                this.scorePosModifier.x = 0;
                                                this.scorePosModifier.y = -1000;

                                                callback ? callback() : "";
                                        }.bind(this)
                                }).start();
                        }
                });
        });