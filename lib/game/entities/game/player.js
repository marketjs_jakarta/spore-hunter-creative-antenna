ig.module('game.entities.game.player')
	.requires(
		'plugins.kvn-utility',
		'game.entities.kvn-delay-execution',

		'game.entities.others.marketjs-entity',

		'game.entities.game-screenshake',

		'game.entities.game.mini-score-notification',
		'game.entities.game.bullet',
		'game.entities.game.upgrade-box',
		'game.entities.game.trap-item',
		'game.entities.game.left-weapon',
		'game.entities.game.front-left-weapon',
		'game.entities.game.front-right-weapon',
		'game.entities.game.right-weapon',
		'game.entities.game.player-shield',
		'game.entities.game.player-explosion',
		'game.entities.game.enemy-explosion',
		'game.entities.game.player-moves-vfx',
		'game.entities.game.player-repair-vfx',
		'game.entities.game.repair-hammer-right',
		'game.entities.game.repair-hammer-left',
		'game.entities.game.repair-button'
	)
	.defines(function () {
		EntityPlayer = EntityMarketjsEntity.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/plane.png'), sheetX: 6, sheetY: 6 },
			gravityFactor: 0,
			zIndex: 10,
			type: ig.Entity.TYPE.A,
			checkAgainst: ig.Entity.TYPE.BOTH,
			health: 3,

			// game logic properties
			barFrameRepairing: new ig.Image('media/graphics/sprites/game/repairing-bar-frame.png'),
			barFillRepairing: new ig.Image('media/graphics/sprites/game/repairing-bar-fill.png'),
			isMoved: false,
			posClickOffsetX: 0,
			timerShoot: null,
			powerLevel: 1,
			leftWeapon: null,
			frontLeftWeapon: null,
			frontRightWeapon: null,
			rightWeapon: null,
			shield: null,
			movesVfx: null,
			isUsingShield: false,
			timerInvicibility: null,
			shootInterval: 0.07,
			fixSfxIndex: 0,
			playerPosY: 0,		// Pos y in which player supposed to be played at
			timerPlayerMoveIn: null,
			playerMovingIn: false,		// Player is animating move into the stage
			timerDamagedAnimation: null,

			// Repairing
			isBroken: false,
			repairStatus: 0,	// Range from 0 (completely broken) to 100 (fixed)
			timerRepairTextBlink: null,
			savedRepairTextColor: "#ffffff",
			timerSpawnRepairHammer: null,
			modifierSpawnRepairHammer: 0,	// To speed up the spawing
			lastRepairHammer: 0,	// Last repair hammer that come out automatically
			lastRepairHammerTap: 0,	// Last repair hammer that come out when player tap the screen
			repairButton: null,
			repairVfx: null,

			// Movement
			moveInterval: 0.001,	// How fast to update player position
			gradualMoveTimeStep: 0.1,		// Time step to move first
			closeMoveTimeStep: 0.8,			// Time step to stay close to mouse pos x
			moveStep: 0.1,			// Current step (percentage)
			reachMousePosX: false,	// If reached mouse x pos in a move
			lastMouseX: null,
			canMove: false,

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create aimation
				this.idle = new ig.Animation(this.idleSheet, 1, [0]);
				this.damaged = new ig.Animation(this.idleSheet, 0.1, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
				this.repair = new ig.Animation(this.idleSheet, 0.04, [12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]);
				this.currentAnim = this.idle;

				this.offset.x = 0.2 * this.size.x;
				this.offset.y = 0.4 * this.size.y;

				this.size.x = this.size.x * 0.6;
				this.size.y = this.size.y / 2;

				this.timerShoot = new ig.Timer();
				this.timerDamagedAnimation = new ig.Timer();
				this.timerRepairTextBlink = new ig.Timer();
				this.timerInvicibility = new ig.Timer();
				this.timerPlayerMoveIn = new ig.Timer();
				this.timerSpawnRepairHammer = new ig.Timer();

				this.playerPosY = this.pos.y;

				this.enableMoveVfx();

				this.playerMoveIn();
			},

			update: function () {
				if(this.controller.isGamePause || this.controller.isGameOver){
					return;
				}

				// To restrict some action when broken
				if (this.isBroken) {
					this.isMoved = false;

					if(this.repairButton == null){
						var playerMidPos = this.getMiddlePosition();
						this.repairButton = ig.game.spawnEntity(EntityRepairButton, -1000, -1000, { controller: this.controller });
						if(playerMidPos.x > this.controller.rWidthH){
							this.repairButton.setMiddlePosition(playerMidPos.x - this.size.x - this.repairButton.size.x / 2,
								playerMidPos.y + 10);
						}else{
							this.repairButton.setMiddlePosition(playerMidPos.x + this.size.x + this.repairButton.size.x / 2,
								playerMidPos.y + 10);
						}
					}

					if(this.timerSpawnRepairHammer.delta() + this.modifierSpawnRepairHammer > 1){
						// Spawn hammer animation
						var playerMidPos = this.getMiddlePosition();
						var settings = { controller: this.controller };
						var sizeXH = this.size.x / 2;
						if(this.lastRepairHammer == 0){
							ig.game.spawnEntity(EntityRepairHammerRight, playerMidPos.x + sizeXH, playerMidPos.y - this.size.y - 20, settings);
							this.lastRepairHammer = 1;
						}else{
							ig.game.spawnEntity(EntityRepairHammerLeft, playerMidPos.x - sizeXH * 3, playerMidPos.y - this.size.y - 20, settings);
							this.lastRepairHammer = 0;
						}

						this.repairPlane(10);

						this.timerSpawnRepairHammer.reset();
						this.modifierSpawnRepairHammer = 0;
					}
				}
				// Detect if mouse out of bond
				else if (ig.game.io.getClickPos().x < 0 && ig.game.io.getClickPos().x > ig.system.realWidth) {
					this.isMoved = false;
				}

				this.movePlayer();

				if (!this.controller.isGameOver && !this.controller.isGamePause && this.timerShoot.delta() > this.shootInterval && this.canMove) {
					this.shoot();
					this.timerShoot.reset();
				}

				// Repairing the plane
				if(this.isBroken && ig.input.pressed('click')){
					if(this.currentAnim != this.repair){
						this.currentAnim = this.repair;
					}

					// Heal vfx on plane
					if(this.repairVfx == null){
						var playerMidPos = this.getMiddlePosition();
						this.repairVfx = ig.game.spawnEntity(EntityPlayerRepairVfx, -1000, -1000, { controller: this.controller, zIndex: this.zIndex + 1 });
						this.repairVfx.setMiddlePosition(playerMidPos.x, playerMidPos.y);
					}

					this.repairPlane(10);

					// Spawn hammer animation
					var playerMidPos = this.getMiddlePosition();
					var settings = { controller: this.controller };
					var sizeXH = this.size.x / 2;
					if(this.lastRepairHammerTap == 0){
						ig.game.spawnEntity(EntityRepairHammerRight, playerMidPos.x, playerMidPos.y - this.size.y - 50, settings);
						this.lastRepairHammerTap = 1;
					}else{
						ig.game.spawnEntity(EntityRepairHammerLeft, playerMidPos.x - sizeXH * 2, playerMidPos.y - this.size.y - 50, settings);
						this.lastRepairHammerTap = 0;
					}
					this.modifierSpawnRepairHammer += 0.5;
				}

				if(this.isUsingShield){
					if(this.timerInvicibility.delta() > 5){
						this.disableShield();
					}
				}

				this.parent();
			},

			check: function(other){
				if(other instanceof EntityUpgradeBox){
					if(this.powerLevel < 3){
						this.upgradePower();
					}else{
						this.controller.addScore(15);

						var playerMidPos = this.getMiddlePosition();
						if(playerMidPos.x > this.controller.rWidthH){
							ig.game.spawnEntity(EntityMiniScoreNotification, this.pos.x - 10, this.pos.y - 10, { text: "+15", controller: this.controller });
						}else{
							ig.game.spawnEntity(EntityMiniScoreNotification, this.pos.x + this.size.x + 10, this.pos.y - 10, { text: "+15", controller: this.controller });
						}

						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.smallBonus);
					}
					other.kill();
				}
				if(other instanceof EntityTrapItem){
					other.trapActivate();
				}
			},

			/**
			 * Request to move from button-move.js
			 */
			requestToMove: function(){
				if (!this.controller.isGameOver && !this.controller.isGamePause	&& !this.isBroken &&
					ig.game.io.getClickPos().x > 10 && ig.game.io.getClickPos().x < (ig.system.realWidth - 10)) {
					this.isMoved = true;
					this.posClickOffsetX = ig.game.io.getClickPos().x - this.pos.x;
				}
			},

			/**
			 * Request stop moving from button-move.js
			 */
			requestToStop: function(){
				this.isMoved = false;
			},

			movePlayer: function () {
				// Animating player move in the stage
				if(this.playerMovingIn){
					var t = this.timerPlayerMoveIn.delta();
					if(t > 1){
						t = 1;
						this.playerMovingIn = false;
						this.canMove = true;
					}
					this.pos.y = ig.KvnUtility.lerp(this.playerPosY + 200, this.playerPosY, t);
				}

				if (this.isMoved && this.canMove) {
					var mouseX = ig.game.io.getClickPos().x;
					// var endPosX = mouseX - this.posClickOffsetX;
					var endPosX = mouseX - this.size.x / 2;
					var leftPosLimit = 5;
					var rightPosLimit = ig.system.width - this.size.x - 5;
					if (endPosX < leftPosLimit) {
						endPosX = leftPosLimit;
					}
					else if (endPosX > rightPosLimit) {
						endPosX = rightPosLimit;
					}
					// this.pos.x = endPosX;

					// If position change
					if(this.lastMouseX == null || this.lastMouseX != mouseX){
						// When not reaching mouse pos x position (after a click on the game)
						if(!this.reachMousePosX){
							// Gradually moves the plane to the mouse pos x
							this.pos.x = ig.KvnUtility.lerp(this.pos.x, endPosX, this.gradualMoveTimeStep);
						}
						// After once reach the mouse pos x
						else{
							// Stick the plane to the mouse pos x
							this.pos.x = ig.KvnUtility.lerp(this.pos.x, endPosX, this.closeMoveTimeStep);
						}
						this.moveStep = this.gradualMoveTimeStep;	// Always reset this variable
					}
					else{
						this.moveStep += this.gradualMoveTimeStep;
						if(this.moveStep > 1){
							this.moveStep = 1;
							this.reachMousePosX = true;
						}
						this.pos.x = ig.KvnUtility.lerp(this.pos.x, endPosX, this.moveStep);
					}

					this.lastMouseX = mouseX;

					// Kill the tutorial (if exists)
					this.controller.killTutorial();
				}
				else{
					this.lastMouseX = null;
					this.reachMousePosX = false;
				}
			},

			shoot: function () {
				if (this.isMoved) {
					var bulletSpeed = 1300;
					if (this.powerLevel == 1) {
						var settings = { vel: { x: 0, y: -bulletSpeed }, controller: this.controller };
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2, this.pos.y - 20, settings);
					} else if (this.powerLevel == 2) {
						var settings1 = { vel: { x: 0, y: -bulletSpeed }, controller: this.controller };
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2, this.pos.y - 20, settings1);
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2 - 22, this.pos.y - 15, settings1);
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2 + 22, this.pos.y - 15, settings1);

						if(this.frontLeftWeapon == null){
							var settings = { controller: this.controller, parentObj: this };
							this.frontLeftWeapon = ig.game.spawnEntity(EntityFrontLeftWeapon, this.pos.x + 2, this.pos.y - 8, settings);
						}
						if(this.frontRightWeapon == null){
							var settings = { controller: this.controller, parentObj: this };
							this.frontRightWeapon = ig.game.spawnEntity(EntityFrontRightWeapon, this.pos.x + 49, this.pos.y - 8, settings);
						}
					}
					else if (this.powerLevel == 3) {
						var settings1 = { vel: { x: 0, y: -bulletSpeed }, controller: this.controller };
						var settings2 = { vel: { x: -300, y: -bulletSpeed }, controller: this.controller };
						var settings3 = { vel: { x: 300, y: -bulletSpeed }, controller: this.controller };
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2, this.pos.y - 20, settings1);
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2 - 22, this.pos.y - 15, settings1);
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2 + 22, this.pos.y - 15, settings1);
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2 - 35, this.pos.y - 5, settings2);
						ig.game.spawnEntity(EntityBullet, this.pos.x + this.size.x / 2 + 35, this.pos.y - 5, settings3);

						if(this.leftWeapon == null){
							var settings = { controller: this.controller, parentObj: this };
							this.leftWeapon = ig.game.spawnEntity(EntityLeftWeapon, this.pos.x - 11, this.pos.y, settings);
						}
						if(this.rightWeapon == null){
							var settings = { controller: this.controller, parentObj: this };
							this.rightWeapon = ig.game.spawnEntity(EntityRightWeapon, this.pos.x + 62, this.pos.y, settings);
						}
					}

					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.playerShoot);
				}
			},

			repairPlane: function(repairValue){
				this.repairStatus += repairValue;
				if(this.repairStatus >= 100){
					this.repairStatus = 100;
					this.playerFixed();
				}
				if(this.fixSfxIndex == 0){
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.fixing1);
				}else if(this.fixSfxIndex == 1){
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.fixing2);
				}else{
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.fixing3);
					this.fixSfxIndex = 0;
				}
				this.fixSfxIndex++;
			},

			upgradePower: function(){
				if(this.powerLevel < 3){
					this.powerLevel++;
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.upgrade);
				}
			},

			downgradePower: function(){
				if(this.powerLevel > 1){
					this.powerLevel--;
				}

				if(this.powerLevel == 2){
					if(this.leftWeapon != null){
						this.leftWeapon.kill();
						this.leftWeapon = null;
					}

					if(this.rightWeapon != null){
						this.rightWeapon.kill();
						this.rightWeapon = null;
					}
				}
				else if(this.powerLevel == 1){
					if(this.frontLeftWeapon != null){
						this.frontLeftWeapon.kill();
						this.frontLeftWeapon = null;
					}

					if(this.frontRightWeapon != null){
						this.frontRightWeapon.kill();
						this.frontRightWeapon = null;
					}
				}
			},
			
			receiveDamage: function( amount, from ){
				if(this.controller.isGameOver){
					return;
				}
				if(this.isUsingShield){
					return;
				}

				if(!this.isBroken){
					this.health -= amount;
					this.currentAnim = this.damaged;
					this.isBroken = true;
					this.disableMoveVfx();

					if(this.health > 0){
						this.timerDamagedAnimation.reset();
						this.repairStatus = 0;
						this.downgradePower();
						
						this.controller.playerBroken();
						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.hit);

						// Spawn explosion
						var midPos = this.getMiddlePosition();
						ig.game.spawnEntity(EntityPlayerExplosion, midPos.x, midPos.y, { zIndex: this.zIndex + 1, controller: this.controller});
						ig.game.spawnEntity(EntityGameScreenshake, 0, 0, {duration: 1, controller: this.controller});
					}
					else{
						this.kill();
					}
				}
			},

			kill: function(){
				this.controller.gameOver();
				var midPos = this.getMiddlePosition();

				ig.game.spawnEntity(EntityPlayerExplosion, midPos.x, midPos.y, { zIndex: this.zIndex + 1, controller: this.controller});
				ig.game.spawnEntity(EntityGameScreenshake, 0, 0, {duration: 1, controller: this.controller});
				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.explode);

				ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
					.setTimeOut(0.4, function () {
						ig.game.spawnEntity(EntityPlayerExplosion, midPos.x, midPos.y, { zIndex: this.zIndex + 1, controller: this.controller});
						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.explode);
					}.bind(this));

				ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
					.setTimeOut(0.8, function () {
						ig.game.spawnEntity(EntityPlayerExplosion, midPos.x + 30, midPos.y + 30, { zIndex: this.zIndex + 1, controller: this.controller });
						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.explode);
					}.bind(this));

				ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
					.setTimeOut(1.2, function () {
						ig.game.spawnEntity(EntityPlayerExplosion, midPos.x - 30, midPos.y - 30, { zIndex: this.zIndex + 1, controller: this.controller });
						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.explode);
					}.bind(this));

				ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
					.setTimeOut(1.6, function () {
						ig.game.spawnEntity(EntityPlayerExplosion, midPos.x - 30, midPos.y + 30, { zIndex: this.zIndex + 1, controller: this.controller });
						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.explode);
					}.bind(this));

				ig.game.spawnEntity(EntityKvnDelayExecution, 0, 0, { controller: this })
					.setTimeOut(2, function () {
						ig.game.spawnEntity(EntityPlayerExplosion, midPos.x + 30, midPos.y - 30, { zIndex: this.zIndex + 1, controller: this.controller });
						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.explode);
					}.bind(this));

				// this.parent();
			},
			
			playerFixed: function(){
				this.isBroken = false;
				this.currentAnim = this.idle;
				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.revive);
				
				this.enableShield();
				this.enableMoveVfx();
				this.controller.playerFixed();
				this.repairButton.kill();
				this.repairButton = null;

				if(this.repairVfx != null){
					this.repairVfx.kill();
					this.repairVfx = null;
				}
			},

			enableShield: function(){
				if(this.shield != null){
					console.log("Shield already enabled");
					return;
				}

				var settings = { controller: this.controller, parentObj: this };
				var topPos = this.getTopMiddlePosition();
				var shieldPos = new Vector2(topPos.x - 98, topPos.y - 60);
				this.shield = ig.game.spawnEntity(EntityPlayerShield, shieldPos.x, shieldPos.y, settings);

				this.isUsingShield = true;
				this.shieldDurability = 5;
				this.timerInvicibility.reset();
			},

			disableShield: function(){
				this.isUsingShield = false;
				this.shield.kill();
				this.shield = null;
			},

			enableMoveVfx: function(){
				if(this.movesVfx != null){
					console.log("Moves Vfx already enabled");
					return;
				}

				var settings = { controller: this.controller, parentObj: this };
				var bottomPos = this.getBottomMiddlePosition();
				var vfxPos = new Vector2(bottomPos.x, bottomPos.y);
				// this.movesVfx = ig.game.spawnEntity(EntityPlayerMovesVfx, vfxPos.x, vfxPos.y, settings);
				this.movesVfx = ig.game.spawnEntity(EntityPlayerMovesVfx, -1000, -1000, settings);
				this.movesVfx.setTopMiddlePosition(bottomPos.x, bottomPos.y - 15);
			},

			disableMoveVfx: function(){
				if(this.movesVfx == null){
					return;
				}

				this.movesVfx.kill();
				this.movesVfx = null;
			},

			draw: function(){
				this.parent();

				if(this.controller.isGameOver){
					return;
				}

				var ContextCanvas = ig.system.context;
				ContextCanvas.save();

				if(this.isBroken){
					// Setting up the repair bar
					var playerBottomMiddlePos = this.getBottomMiddlePosition();
					var posX = playerBottomMiddlePos.x - this.barFrameRepairing.width / 2 - ig.game.screen.x;
					var posY = playerBottomMiddlePos.y + 10 - ig.game.screen.y;
					var fillWidth = this.barFrameRepairing.width * this.repairStatus / 100;

					// Draw the frame
					this.barFrameRepairing.draw(posX, posY);

					if(fillWidth > 0){
						// Draw the fill
						this.barFillRepairing.draw(posX, posY, 0, 0, fillWidth, this.barFrameRepairing.height);
					}

					// Setting up the text
					var fontSize = 20;
					var fontStyle = "porkys";
					var text = _STRINGS["Game"]["TapToFix"];
					var repairTextColorBlinkLight = "#ffffff";
					var repairTextColorBlinkDark = "#42acf8";

					if(this.timerRepairTextBlink.delta() > 0.15){
						if(this.savedRepairTextColor == repairTextColorBlinkLight){
							this.savedRepairTextColor = repairTextColorBlinkDark;
						}
						else{
							this.savedRepairTextColor = repairTextColorBlinkLight;
						}
						this.timerRepairTextBlink.reset();
					}
					ContextCanvas.fillStyle = this.savedRepairTextColor;
					ContextCanvas.font = "bold " + fontSize + "px " + fontStyle;
					ContextCanvas.textAlign = 'center';
					ContextCanvas.textBaseline = "alphabetic";
					ContextCanvas.fillText(text, playerBottomMiddlePos.x - ig.game.screen.x, posY + this.barFrameRepairing.height + 20);
				}

				ContextCanvas.restore();
			},

			// Player animation at the start of the game
			playerMoveIn: function(){
				// Not already moving in
				if(!this.playerMovingIn){
					this.playerMovingIn = true;
					this.timerPlayerMoveIn.reset();
					this.canMove = false;
				}
			}
		});
	});