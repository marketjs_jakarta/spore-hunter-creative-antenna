ig.module('game.entities.game.right-weapon')
	.requires(
		'game.entities.game.attached-weapon'
	)
	.defines(function () {
		EntityRightWeapon = EntityAttachedWeapon.extend({
	            idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/right-weapon.png'), sheetX: 1, sheetY: 1 }
		});
	});