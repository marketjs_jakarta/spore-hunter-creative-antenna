ig.module(
    'game.entities.game.player-explosion'
)
    .requires(
        'impact.entity-pool'
    )
    .defines(function () {

        EntityPlayerExplosion = EntityMarketjsEntity.extend({
            gravityFactor: 0,

            idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/player-explosion.png'), sheetX: 3, sheetY: 3 },

            timerLifetime: null,

            init: function (x, y, settings) {
                this.parent(x, y, settings);

                // create aimation
				this.idle = new ig.Animation(this.idleSheet, 0.1, [0,1,2,3,4,5,6]);
				this.currentAnim = this.idle;

                this.lifeTimer = new ig.Timer();
                this.pos.x = this.pos.x - this.size.x / 2;
                this.pos.y = this.pos.y - this.size.y / 2;
            },

            reset: function (x, y, settings) {
                this.parent(x, y, settings);

                this.currentAnim = this.idle.rewind();

                this.lifeTimer = new ig.Timer();
                this.pos.x = this.pos.x - this.size.x / 2;
                this.pos.y = this.pos.y - this.size.y / 2;
            },

            update: function () {
                // This method is called for every frame on each entity.
                // React to input, or compute the entity's AI here.

                if (this.lifeTimer.delta() > 0.7) {
                    this.kill();
                }

                // Call the parent update() method to move the entity
                // according to its physics
                this.parent();
            }
        });

        ig.EntityPool.enableFor(EntityPlayerExplosion);

    });
