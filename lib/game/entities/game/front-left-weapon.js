ig.module('game.entities.game.front-left-weapon')
	.requires(
        'game.entities.game.attached-weapon'
	)
	.defines(function () {
		EntityFrontLeftWeapon = EntityAttachedWeapon.extend({
            idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/front-left-weapon.png'), sheetX: 1, sheetY: 1 }
		});
	});