ig.module('game.entities.game.attached-weapon')
	.requires(
        'impact.entity-pool',

        'game.entities.others.marketjs-entity',
        'plugins.data.vector'
	)
	.defines(function () {
		EntityAttachedWeapon = EntityMarketjsEntity.extend({
            offsetPos: null,

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create animation
                this.idle = new ig.Animation(this.idleSheet, 1, [0]);
                this.currentAnim = this.idle;

                this.offsetPos = new Vector2(settings.parentObj.pos.x - this.pos.x, settings.parentObj.pos.y - this.pos.y);
            },

            reset: function (x, y, settings) {
				this.parent(x, y, settings);

                this.offsetPos = new Vector2(settings.parentObj.pos.x - this.pos.x, settings.parentObj.pos.y - this.pos.y);
            },
            
            update: function () {
				this.parent();

                this.pos.x = this.parentObj.pos.x - this.offsetPos.x;
                this.pos.y = this.parentObj.pos.y - this.offsetPos.y;
            }
        });
        
        // With pooling enabled, instances that are removed from the game world are not
		// completely erased, but rather put in a pool and resurrected when needed.

        ig.EntityPool.enableFor(EntityAttachedWeapon);
        
	});