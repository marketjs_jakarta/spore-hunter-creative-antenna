ig.module('game.entities.game.trap-item')
	.requires(
		'plugins.kvn-scale',	// don't use it together with plugins.scale

		'game.entities.others.marketjs-entity',
		'game.entities.game.loot-drop'
	)
	.defines(function () {
		/**
		 * Obsolete. Can safely delete.
		 */
		EntityTrapItem = EntityLootDrop.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/saw.png'), sheetX: 1, sheetY: 1 },
			isActivated: false,
			velX: 0,

			init: function (x, y, settings) {
				this.parent(x, y, settings);
				
				this.offset.x = 0.2 * this.size.x;
				this.offset.y = 0.2 * this.size.y;

				this.size.x = this.size.x * 0.6;
				this.size.y = this.size.y * 0.6;

				this.scaleInit();
				this.setScale(0.3, 0.3);
				this.scaling = true;
				this.bodyScale = 0.3;
				this.checkAgainst= ig.Entity.TYPE.NONE;
			},

			update: function () {
				if(this.controller.isGamePause){
					return;
				}

				this.parent();

				if(this.isTouchGround() && !this.isActivated){
					this.gravityFactor = 0;
					this.vel.x = 0;
					this.vel.y = 0;
				} else if(this.isTouchGround()){
					this.vel.x = this.velX;
					this.vel.y = -600;
				}

				if(this.isActivated){
					this.gravityFactor = 1;

					if(this.pos.x < -this.size.x){
						this.kill();
					}else if(this.pos.x > this.controller.rWidth){
						this.kill();
					}
				}
			},

			trapActivate: function () {
				if (!this.isActivated) {
					this.isActivated = true;
					this.vel.y = -600;
					this.tween({ bodyScale: 1 }, 1, {
						easing: ig.Tween.Easing.Linear.EaseNone,
						onComplete: function () {
							var midPos = this.getMiddlePosition();
							if (midPos.x < this.controller.rWidthH) {
								this.velX = 40;
							} else {
								this.velX = -40;
							}

							this.checkAgainst = ig.Entity.TYPE.A;
							this.type = ig.Entity.TYPE.B;
						}.bind(this)
					}).start();
				}
			},

			check: function (other) {
				if (other instanceof EntityPlayer) {
					other.receiveDamage(1, this);
				}
			}
		});
	});