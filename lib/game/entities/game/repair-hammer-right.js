ig.module('game.entities.game.repair-hammer-right')
	.requires(
        'impact.entity-pool',
        'game.entities.game.repair-hammer',

        'game.entities.others.marketjs-entity',
        'plugins.data.vector'
	)
	.defines(function () {
		EntityRepairHammerRight = EntityRepairHammer.extend({

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// Configure animation
                this.currentAnim.pivot.x = this.size.x * 0.8;
                this.currentAnim.pivot.y = this.size.y * 0.8;

                this.rotation = 0.4;
                this.tweenPull();
            },

            reset: function (x, y, settings) {
				this.parent(x, y, settings);

            },
            
            update: function () {
				this.parent();

                
            },

            tweenPull: function(){
                this.tween({rotation: 0.7}, 0.1, {
                    easing: ig.Tween.Easing.Linear.EaseNone,
                    onComplete:function(){
                        this.tweenHit();
                    }.bind(this)
                }).start();
            },

            tweenHit: function(){
                this.tween({rotation: -0.7}, 0.3, {
                    easing: ig.Tween.Easing.Linear.EaseNone,
                    onComplete:function(){
                        this.kill();
                    }.bind(this)
                }).start();
            }
        });
        
	});