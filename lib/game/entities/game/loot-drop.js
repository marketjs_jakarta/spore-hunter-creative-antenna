ig.module('game.entities.game.loot-drop')
	.requires(
		'game.entities.others.marketjs-entity'
	)
	.defines(function () {
		EntityLootDrop = EntityMarketjsEntity.extend({
			zIndex: 20,
			gravityFactor: 0.8,
			maxVel: { x: 1000, y: 1000 },

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				this.type = ig.Entity.TYPE.B;	// Enemy group at first to be able to be shot by player

				// create aimation
                this.idle = new ig.Animation(this.idleSheet, 1, [0]);
				this.currentAnim = this.idle;
			},

			update: function () {
				this.parent();

				if(this.isTouchGround()){
					this.gravityFactor = 0;
					this.vel.x = 0;
					this.vel.y = 0;
				}
			},

			isTouchGround: function(){
				var bottomPos = this.pos.y + this.size.y;
				if(bottomPos > this.controller.groundPosY){
					return true;
				}
				else{
					return false;
				}
			},

			gotShot: function(){
				if(this.parentObj == null){
					return;
				}

				// Prevent the box dropped out of the screen
				if(this.pos.x > this.size.x){
					this.type = ig.Entity.TYPE.A;	// Player group to be picked by player
					this.parentObj.isLootDropped = true;
					this.parentObj = null;
				}
			}
		});
	});