ig.module('game.entities.game.game-level')
	.requires(
        'plugins.kvn-utility',

        'game.entities.others.marketjs-entity',
        'game.entities.game.enemy-green',
        'game.entities.game.enemy-orange',
        'game.entities.game.enemy-red'
	)
	.defines(function () {
		EntityGameLevel = EntityMarketjsEntity.extend({
            enemiesSet: [],
            currentPointer: 0,

			init: function (x, y, settings) {
				this.parent(x, y, settings);
            },
            
            /**
             * Fills enemiesSet array to be used by game controller
             */
            setEnemies: function(){
                var set = [];

                switch(this.controller.wave){
                    case 1:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 3, maxHealth: 5},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 3, maxHealth: 10},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 15}
                    ];
                    break;

                    case 2:
                    set = [
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 15, maxHealth: 20},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 10, maxHealth: 15},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.big, minHealth: 35, maxHealth: 40},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 30}
                    ];
                    break;

                    case 3:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 20},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 30, maxHealth: 40},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.big, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.large, minHealth: 30, maxHealth: 35},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 15},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 30, maxHealth: 35},
                    ];
                    break;

                    case 4:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.big, minHealth: 10, maxHealth: 15},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.small, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.medium, minHealth: 80, maxHealth: 100},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.small, minHealth: 50, maxHealth: 60},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 15},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 10},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.big, minHealth: 50, maxHealth: 60},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 15},
                    ];
                    break;

                    case 5:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 25},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.small, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.large, minHealth: 30, maxHealth: 35},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.small, minHealth: 30, maxHealth: 35},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.small, minHealth: 120, maxHealth: 130},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 50, maxHealth: 60},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 15},
                    ];
                    break;

                    case 6:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 25},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 15},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.large, minHealth: 30, maxHealth: 35},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.small, minHealth: 120, maxHealth: 130},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 3, maxHealth: 5},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                    ];
                    break;

                    case 7:
                    set = [
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.small, minHealth: 30, maxHealth: 40},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 15},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.medium, minHealth: 190, maxHealth: 200},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 3, maxHealth: 5},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 15},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.large, minHealth: 70, maxHealth: 80},
                    ];
                    break;

                    case 8:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 10, maxHealth: 15},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.big, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 15},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.medium, minHealth: 190, maxHealth: 200},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 3, maxHealth: 5},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 10, maxHealth: 15},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.small, minHealth: 90, maxHealth: 100},
                    ];
                    break;

                    case 9:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 10, maxHealth: 15},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 15},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.big, minHealth: 250, maxHealth: 300},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 3, maxHealth: 5},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 40, maxHealth: 45},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 15, maxHealth: 20},
                    ];
                    break;

                    case 10:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.large, minHealth: 50, maxHealth: 60},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.small, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.small, minHealth: 200, maxHealth: 250},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 15},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.medium, minHealth: 150, maxHealth: 200},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 3, maxHealth: 5},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 40, maxHealth: 45},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 15, maxHealth: 20},
                    ];
                    break;

                    case 11:
                    set = [
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.large, minHealth: 90, maxHealth: 100},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 10},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.medium, minHealth: 200, maxHealth: 250},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 15},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 3, maxHealth: 5},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.medium, minHealth: 175, maxHealth: 200},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 40, maxHealth: 45},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 15, maxHealth: 20},
                    ];
                    break;

                    default:
                    set = [
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.large, minHealth: 50, maxHealth: 60},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.small, minHealth: 50, maxHealth: 55},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.small, minHealth: 200, maxHealth: 250},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 5, maxHealth: 15},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 20, maxHealth: 25},
                        {enemyEntity: EntityEnemyRed, enemySize: this.controller.enemyVariation.medium, minHealth: 150, maxHealth: 200},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.small, minHealth: 3, maxHealth: 5},
                        {enemyEntity: EntityEnemyOrange, enemySize: this.controller.enemyVariation.medium, minHealth: 40, maxHealth: 45},
                        {enemyEntity: EntityEnemyGreen, enemySize: this.controller.enemyVariation.medium, minHealth: 15, maxHealth: 20},
                    ];
                    break;
                }
                this.currentPointer = 0;
                this.enemiesSet = set;
            },

            /**
             * Get enemies config from enemiesSet array while incrementing the pointer.
             */
            getEnemiesConfig: function(){
                if(this.enemiesSet.length > this.currentPointer){
                    var config = this.enemiesSet[this.currentPointer];
                    this.currentPointer++;
                    return config;
                }

                return null;
            }
		});
	});