ig.module('game.entities.game.pop-up-gameover')

.requires(	
	'plugins.kvn-utility',
	'plugins.textwrapper',

	'game.entities.others.marketjs-entity',
	'game.entities.buttons.button',
	'game.entities.buttons.button-home',
	'game.entities.buttons.button-restart'
)

.defines(function() {

	EntityPopUpGameover = EntityMarketjsEntity.extend({
		// impact entity properties
		gravityFactor: 0,
		zIndex: 50,
		// marketjs entity propertis
		idleSheetInfo: { sheetImage:new ig.Image('media/graphics/sprites/game/game-over-window.png'), sheetX:1, sheetY:1 },
		// other entity properties
		titleText: new ig.Image('media/graphics/sprites/game/game-over-text.png'),
		buttonRestart: null,
		buttonHome: null,

		init: function( x, y, settings ) {
			
			this.parent( x, y, settings );

			// create animation
			this.idle = new ig.Animation(this.idleSheet, 1, [0]);
			this.currentAnim = this.idle;

			// set position horizontally and vertically
			this.setMiddlePosition(settings.controller.rWidthH, settings.controller.rHeightH);
			
			// set position before first tween
			this.pos.y += ig.system.height;

			// create entities
			this.blackBg = ig.game.spawnEntity(EntityBlackBackground, 0, 0, {controller: this.controller, zIndex:this.zIndex-1});
			this.buttonRestart = ig.game.spawnEntity(EntityButtonRestart, ig.system.width, ig.system.height, {controller: this.controller, zIndex:this.zIndex+1});
			this.buttonHome = ig.game.spawnEntity(EntityButtonHome, ig.system.width, ig.system.height, {controller: this.controller, zIndex:this.zIndex+1});
		},

		update: function() {

			this.parent();
			
			// set position entities
			this.buttonRestart.pos.x = this.pos.x+this.size.x*1/4 - this.buttonRestart.size.x*1/2;
			this.buttonRestart.pos.y = this.pos.y+this.size.y*26/20 - this.buttonRestart.size.y*1/2 + this.buttonRestart.offsetY;
			this.buttonHome.pos.x = this.pos.x+this.size.x*3/4 - this.buttonHome.size.x*1/2;
			this.buttonHome.pos.y = this.pos.y+this.size.y*26/20 - this.buttonHome.size.y*1/2 + this.buttonHome.offsetY;
		},

		draw: function() {

			this.parent();

			ig.system.context.save();

			// title
			var posTitle = {x: this.pos.x+this.size.x*1/2-this.titleText.width/2, y: this.pos.y+this.size.y*3.5/20};
			this.titleText.draw(posTitle.x, posTitle.y);

			var ContextCanvas = ig.system.context;
			var fontSize = 55;
			var margin = 30;
			var startX = this.pos.x + this.size.x/2;
			var startY = posTitle.y + fontSize + margin + 40;
			var strokeStyle = "#000000";
			var lineWidth = 2;
			var shadowColor = "#000000";
			var shadowBlur = 4;
			var shadowOffsetX = 3;
			var shadowOffsetY = 3;
			var maxWidth = 500;

			// Score Text
			var text = _STRINGS["Game"]["Score"] + ": " + parseInt(this.controller.score || 0).toLocaleString();
			ContextCanvas.textAlign = 'left';
			ContextCanvas.font = "bold " + fontSize + "px porkys";
			ContextCanvas.fillStyle = "#ffffff";
			// ig.KvnUtility.drawStrokedText(ContextCanvas, text, startX, startY, strokeStyle, lineWidth);

			var textwrapper = new ig.Textwrapper(fontSize, "porkys");
			var textlist = textwrapper.wrapText(text, maxWidth);
			textwrapper.textSpacing = 2;
			textwrapper.textAlign = 'center';
			textwrapper.drawStrokedTextList(textlist, startX, startY);

			// Highscore text
			startY += fontSize + margin;
			fontSize = 45;
			ContextCanvas.font = "bold " + fontSize + "px porkys";
			ContextCanvas.textAlign = 'left';
			text = _STRINGS["Game"]["Highscore"] + ": " + parseInt(this.controller.getHighscore() || 0).toLocaleString();
			// ig.KvnUtility.drawStrokedText(ContextCanvas, text, startX, startY, strokeStyle, lineWidth);

			textwrapper = new ig.Textwrapper(fontSize, "porkys");
			textlist = textwrapper.wrapText(text, maxWidth);
			textwrapper.textSpacing = 2;
			textwrapper.textAlign = 'center';
			textwrapper.drawStrokedTextList(textlist, startX, startY);

            ig.system.context.restore();
		},

		easeOut: function() {
			this.blackBg.easeOut();
			this.tween({pos:{y: this.pos.y-ig.system.height}}, 0.2, {
				easing: ig.Tween.Easing.Linear.EaseNone,
				onComplete:function(){
					this.pos.y += ig.system.height*2;
				}.bind(this)
			}).start();
		},

		easeIn: function() {
			this.blackBg.easeIn();
			this.tween({pos:{y: this.pos.y-ig.system.height}}, 0.2, {
				easing: ig.Tween.Easing.Linear.EaseNone,
				onComplete:function(){
					ig.game.easing = false;
				}.bind(this)
			}).start();
		}
	});
});