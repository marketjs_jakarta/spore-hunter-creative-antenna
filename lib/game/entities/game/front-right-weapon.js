ig.module('game.entities.game.front-right-weapon')
	.requires(
		'game.entities.game.attached-weapon'
	)
	.defines(function () {
		EntityFrontRightWeapon = EntityAttachedWeapon.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/front-right-weapon.png'), sheetX: 1, sheetY: 1 }
		});
	});