ig.module('game.entities.game.repair-button')
	.requires(
        'impact.entity-pool',

        'game.entities.others.marketjs-entity',
        'plugins.data.vector'
	)
	.defines(function () {
        /**
         * Only for visual. Not really a button
         */
		EntityRepairButton = EntityMarketjsEntity.extend({
            gravityFactor: 0,
            idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/repair-button.png'), sheetX: 1, sheetY: 1 },

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create animation
                this.idle = new ig.Animation(this.idleSheet, 1, [0]);
                this.currentAnim = this.idle;
            },

            reset: function (x, y, settings) {
				this.parent(x, y, settings);

            },
            
            update: function () {
				this.parent();
            }
        });
        
	});