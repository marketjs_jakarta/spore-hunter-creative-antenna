ig.module('game.entities.game.drone')
	.requires(
		'game.entities.others.marketjs-entity',

		'game.entities.game.upgrade-box',
		'game.entities.game.trap-item'
	)
	.defines(function () {
		EntityDrone = EntityMarketjsEntity.extend({
			zIndex: 20,
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/drone.png'), sheetX: 2, sheetY: 2 },
            gravityFactor: 0,
            movementSpeed: 300,
			timerLifetime: null,
			loot: null,
			isLootDropped: false,
			lootSpawnPos: {x: 63, y: 98},

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create aimation
                this.idle = new ig.Animation(this.idleSheet, 0.05, [0, 1, 2, 3]);
                this.currentAnim = this.idle;
                
                this.vel.x = this.movementSpeed;
                
				this.timerLifetime = new ig.Timer();
				
				if(!ig.global.wm){
					// Create entities
					this.loot = ig.game.spawnEntity(EntityUpgradeBox, this.lootSpawnPos.x, this.lootSpawnPos.y, {controller: this.controller, parentObj: this});
				}
			},

			update: function () {
				if(this.controller.isGamePause){
					return;
				}

				this.parent();
				
				if(!this.isLootDropped){
					this.loot.setTopMiddlePosition(this.pos.x + this.lootSpawnPos.x, this.pos.y + this.lootSpawnPos.y);
					this.loot.vel.y = 100;
				}
				else{
					this.loot = null;
				}
                
                if(this.timerLifetime.delta() > 10){
					if(!this.isLootDropped && this.loot != null){
						this.loot.kill();
					}
                    this.kill();
                }
			}
		});
	});