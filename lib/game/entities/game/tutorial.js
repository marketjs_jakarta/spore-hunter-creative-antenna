ig.module(
    'game.entities.game.tutorial'
)
    .requires(
        'game.entities.others.marketjs-entity'
    )
    .defines(function () {

        EntityTutorial = EntityMarketjsEntity.extend({
            gravityFactor: 0,
            zIndex: 10,
            hand: null,
            bar: null,

            inTransition: false,    // Transition between moving in and out

            init: function (x, y, settings) {
                this.parent(x, y, settings);

                if(!ig.global.wm){
                    // Create entities
                    this.bar = ig.game.spawnEntity(EntityTutorialBar, this.pos.x, this.pos.y, {controller: this.controller, parentObj: this, zIndex: this.zIndex});
                    this.hand = ig.game.spawnEntity(EntityTutorialHand, this.pos.x, this.pos.y + this.bar.size.y - 30, {controller: this.controller, parentObj: this, zIndex: this.zIndex});

                    this.bar.show();
                    this.hand.show();
				}
            },

            kill: function(){
                if(this.bar.isActive && this.hand.isActive){
                    this.bar.kill();
                    this.hand.kill();

                    this.parent();
                    return true;
                }

                return false;
            }
        });

        EntityTutorialBar = EntityMarketjsEntity.extend({
            gravityFactor: 0,

            idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/hand-slide-bar.png'), sheetX: 1, sheetY: 1 },
            originalPos: null,
            bodyScaleX: 1,
            bodyScaleY: 0.8,
            isActive: false,

            init: function (x, y, settings) {
                this.parent(x, y, settings);

                // create aimation
				this.idle = new ig.Animation(this.idleSheet, 1, [0]);
                this.currentAnim = this.idle;

                this.setTopMiddlePosition(this.pos.x, this.pos.y);
                this.originalPos = new Vector2(this.pos.x, this.pos.y);
            },

            update: function(){
                this.parent();

                this.setScale(this.bodyScaleX, this.bodyScaleY);
            },

            show: function (seconds, callback) {
				if (seconds == null) seconds = 0.5;

				// Go back to real position
				this.pos.x = this.originalPos.x;
				this.pos.y = this.originalPos.y;
                this.bodyScaleX = 0;
                this.bodyScaleY = 0;

				this.tween({ bodyScaleX: 1, bodyScaleY: 0.8 }, seconds, {
					easing: ig.Tween.Easing.Back.EaseOut,
					onComplete: function () {
                        this.isActive = true;
						callback ? callback() : "";
					}.bind(this)
				}).start();
			},

			hide: function (seconds, callback) {
				if (seconds == null) seconds = 0.5;

				this.tween({ bodyScaleX: 1, bodyScaleY: 0.8 }, seconds, {
					easing: ig.Tween.Easing.Back.EaseIn,
					onComplete: function () {
                        // Go out screen
				        this.pos.y = -1000;

						callback ? callback() : "";
					}.bind(this)
				}).start();
			}
        });

        EntityTutorialHand = EntityMarketjsEntity.extend({
            gravityFactor: 0,

            idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/finger.png'), sheetX: 1, sheetY: 1 },
            originalPos: null,

            leftTarget: 30,
            rightTarget: 450,

            isActive: false,

            init: function (x, y, settings) {
                this.parent(x, y, settings);

                // create aimation
				this.idle = new ig.Animation(this.idleSheet, 1, [0]);
                this.currentAnim = this.idle;

                this.setTopMiddlePosition(this.pos.x, this.pos.y);
                this.originalPos = new Vector2(this.pos.x, this.pos.y);
                this.scaling = true;
            },

            show: function( seconds, callback ){
                if (seconds == null) seconds = 0.5;

				// Go back to real position
				this.pos.x = this.originalPos.x;
				this.pos.y = this.originalPos.y;
                this.bodyScale = 0;

				this.tween({ bodyScale: 1 }, seconds, {
					easing: ig.Tween.Easing.Back.EaseOut,
					onComplete: function () {
                        this.tweenHand();
                        this.isActive = true;
						callback ? callback() : "";
					}.bind(this)
				}).start();
            },

            hide: function( seconds, callback ){
                if (seconds == null) seconds = 0.5;

                this.bodyScale = 1;

				this.tween({ bodyScale: 0 }, seconds, {
					easing: ig.Tween.Easing.Back.EaseOut,
					onComplete: function () {
                        // Go out of screen
				        this.pos.y = -1000;
                        
						callback ? callback() : "";
					}.bind(this)
				}).start();
            },
            
            /**
             * Start tweening hand from the middle
             */
            tweenHand: function(){
                this.tween({pos: {x: this.rightTarget, y: this.pos.y}}, 1, {
                    easing: ig.Tween.Easing.Linear.EaseNone,
                    onComplete:function() {
                        this.tweenHandLeft();
                    }.bind(this)
                }).start();
            },

            tweenHandLeft: function(){
                this.tween({pos: {x: this.leftTarget, y: this.pos.y}}, 2, {
                    easing: ig.Tween.Easing.Linear.EaseNone,
                    onComplete:function() {
                        this.tweenHandRight();
                    }.bind(this)
                }).start();
            },

            tweenHandRight: function(){
                this.tween({pos: {x: this.rightTarget, y: this.pos.y}}, 2, {
                    easing: ig.Tween.Easing.Linear.EaseNone,
                    onComplete:function() {
                        this.tweenHandLeft();
                    }.bind(this)
                }).start();
            }
        });
    });
