ig.module('game.entities.game.game-background')
	.requires(
        'plugins.kvn-utility',

		'game.entities.others.marketjs-entity'
	)
	.defines(function () {
		EntityGameBackground = EntityMarketjsEntity.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/game-bg.png'), sheetX: 1, sheetY: 1 },
            gravityFactor: 0,
            startPos: null,
            timerScroll: null,
            scrollTime: 12,
            isPaused: false,
            stopPosY: [-960, 0, 960],
            stopPosBefore: 0,   // array index
            currentTargetStop: 1,   // array index

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create aimation
                this.idle = new ig.Animation(this.idleSheet, 1, [0]);
                this.currentAnim = this.idle;

                this.timerScroll = new ig.Timer();
			},

			update: function () {
				if(this.controller.isGamePause){
					return;
                }

                this.parent();
                
                var t = this.timerScroll.delta() / this.scrollTime;
                if(t > 1){
                    t = 1;
                    this.timerScroll.reset();
                }

                var newPosY = ig.KvnUtility.lerp(this.stopPosY[this.stopPosBefore], this.stopPosY[this.currentTargetStop], t);
                this.pos.y = newPosY;

                if(t == 1){
                    if(this.currentTargetStop == 2){
                        this.currentTargetStop = 1;
                        this.stopPosBefore = 0;
                    }
                    else{
                        this.stopPosBefore = this.currentTargetStop;
                        this.currentTargetStop++;
                    }
                }
            },
            
            pause: function(){
                this.timerScroll.pause();
            },

            unpause: function(){
                this.timerScroll.unpause();
            }
		});
	});