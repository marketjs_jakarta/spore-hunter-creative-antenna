ig.module('game.entities.game.enemy-special')
	.requires(
		'impact.entity-pool',
		'plugins.kvn-scale',	// don't use it together with plugins.scale
		'plugins.kvn-utility',

		'game.entities.others.marketjs-entity'
	)
	.defines(function () {
		EntityEnemySpecial = EntityMarketjsEntity.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/saw.png'), sheetX: 1, sheetY: 1 },
			gravityFactor: 0.8,
			zIndex: 4,
			type: ig.Entity.TYPE.B,	// Enemy group
			checkAgainst: ig.Entity.TYPE.A,
			maxVel: { x: 800, y: 800 },
			savedVelocity: 0,	// Velocity before damaged and slowed down
			reboundSpeed: 600,
			topMarginPosition: 250,	// Above this position, bullets can't push it up
			isGoingDown: null,	// If false, its going up
			hasTouchedGround: false,	// At least touch ground once

			// Rotatino
			rotation: 0,	// In radian
			rotateDuration: 3,	// Duration each rotation takes
			timerRotation: null,

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create animation
				this.idle = new ig.Animation(this.idleSheet, 1, [0]);
				this.currentAnim = this.idle;

				this.pos.x = this.pos.x - this.size.x / 2;

				// Make the collider fit to the image
				this.offset.x = 0.2 * this.size.x;
				this.offset.y = 0.2 * this.size.y;

				this.size.x = this.size.x * 0.6;
				this.size.y = this.size.y * 0.6;

				this.scaleInit();
				this.setScale(1.2, 1.2);
				this.isGoingDown = true;

				this.timerRotation = new ig.Timer();
			},

			reset: function (x, y, settings) {
				this.parent(x, y, settings);

				this.pos.x = this.pos.x - this.size.x / 2;

				this.isGoingDown = true;
			},

			update: function () {
				if(this.controller.isGamePause){
					return;
				}

				this.parent();

				var bottomLimitPos = this.controller.rHeight - this.size.y;
				if (this.pos.y > bottomLimitPos) {
					this.vel.y = -this.reboundSpeed;
					this.isGoingDown = false;
					this.hasTouchedGround = true;
				}

				if (this.hasTouchedGround) {
					if (this.pos.x < -this.size.x - 50) {
						this.kill();
					} else if (this.pos.x > this.controller.rWidth + 50) {
						this.kill();
					}
				}

				// When is going up but the velocity already bigger than zero
				// then we know it's going down
				if(!this.isGoingDown){
					if(this.vel.y > 0){
						this.isGoingDown = true;
					}
				}

				// The rotation

				var t = this.timerRotation.delta() / this.rotateDuration;
				if(t > 1){
					t = 1;
					this.timerRotation.reset();
				}
				var rotationDeg = ig.KvnUtility.lerp(0, 360, t);
				this.rotation = rotationDeg.toRad();
				this.currentAnim.angle = this.rotation;
			},

			check: function ( other ){
				if(other instanceof EntityPlayer){
					other.receiveDamage( 1, this );
				}
			},

			receiveDamage: function( amount, from ){
				// Nothing
			}
		});
		ig.EntityPool.enableFor(EntityEnemySpecial);
	});