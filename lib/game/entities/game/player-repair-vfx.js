ig.module('game.entities.game.player-repair-vfx')
	.requires(
		'game.entities.others.marketjs-entity'
	)
	.defines(function () {
		EntityPlayerRepairVfx = EntityMarketjsEntity.extend({
			gravityFactor: 0,
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/player-heal.png'), sheetX: 6, sheetY: 4 },

			init: function (x, y, settings) {
				this.parent(x, y, settings);

				// create animation
				this.idle = new ig.Animation(this.idleSheet, 0.04, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
				this.currentAnim = this.idle;
			}
		});
	});