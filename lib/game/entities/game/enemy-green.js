ig.module('game.entities.game.enemy-green')
	.requires(
		'impact.entity-pool',
		'plugins.scale',

		'game.entities.game.enemy',
		'game.entities.game.enemy-explosion'
	)
	.defines(function () {
		EntityEnemyGreen = EntityEnemy.extend({
			idleSheetInfo: { sheetImage: new ig.Image('media/graphics/sprites/game/green-spore.png'), sheetX: 8, sheetY: 5 },

			// Animation stuff
			healthLimit1: 20,
			healthLimit2: 50,
			healthLimit3: 90
		});
	});