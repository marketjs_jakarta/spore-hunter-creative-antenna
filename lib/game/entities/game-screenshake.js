ig.module('game.entities.game-screenshake')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityGameScreenshake = ig.Entity.extend({

		duration:0.5,
		strength:5,

		init:function(x,y,settings){
			this.parent(x,y,settings);

			// init stuff here
			this.initShock();

			var instances = ig.game.getEntitiesByType(EntityGameScreenshake);
			if(instances.length > 1) {
				this.kill();	
			}
		},

		initShock:function(){
			this.shockTimer=new ig.Timer();
			this.shockTimer.set(this.duration);                    
		},

		resetShock:function(){
			this.shockTimer = null;
			ig.game.screen.x=0;
			ig.game.screen.y=0;
			this.kill();
		},

		shock:function(){
			var delta=this.shockTimer.delta();
			if(delta<-0.2){
				var s=this.strength*Math.pow(-delta/this.duration,2);
				if(s>0.5){
					ig.game.screen.x+=Math.random().map(0,1,-s,s);
					ig.game.screen.y+=Math.random().map(0,1,-s,s);
				}
			}

			if(delta>0){ //after timer runs out
				this.resetShock();
			}
		},

		update:function(){
			this.parent();
			this.shock();
		},

		draw:function(){
			this.parent();
		}
	});
});