ig.module('game.entities.kvn-delay-execution')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityKvnDelayExecution = ig.Entity.extend({

		timer: null,
		delay: null,
		callback: null,

		init:function(x,y,settings){
			this.parent(x,y,settings);
		},

		setTimeOut: function( delay, callback ){
			this.timer = new ig.Timer();
			this.delay = delay;
			this.callback = callback;
		},

		update:function(){
			this.parent();
			
			if(this.timer.delta() > this.delay){
				this.callback ? this.callback() : "";

				this.kill();
			}
		},
	});
});