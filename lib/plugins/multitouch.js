ig.module( 'plugins.multitouch' )
.requires(
  'impact.game',
  'impact.input'
)
.defines(function() {

  var TouchPoint = function( x, y, id, state ) {
    this.x = x
    this.y = y
    this.id = id
    this.state = state
  };

  ig.Input.inject({

    touches: {},
    delayedTouchUp: [],
      fingerCount: [],
    // unfortunally we have to overwrite this function completly without calling this.parent()
    // the touch events provided by impact get in the way so better drop them
    // this could lead to some incompatibility, so keep that in mind when updating impact
    initMouse: function() {
      if( this.isUsingMouse ) { return; }
      this.isUsingMouse = true;

      // This works with the iOSImpact, too
      // Just remeber to copy the provided JS_TouchInput.h and JS_TouchInput.m
      if ( typeof( ios ) != 'undefined' && ios ) {
        this._touchInput = new native.TouchInput();
        this._touchInput.touchStart( this.multitouchstart.bind(this) );
        this._touchInput.touchEnd( this.multitouchend.bind(this) );
        this._touchInput.touchMove( this.multitouchmove.bind(this) );
      }
      else {
        var mouseWheelBound = this.mousewheel.bind(this);
        ig.system.canvas.addEventListener('mousewheel', mouseWheelBound, false );
        ig.system.canvas.addEventListener('DOMMouseScroll', mouseWheelBound, false );

        ig.system.canvas.addEventListener('contextmenu', this.contextmenu.bind(this), false );
        ig.system.canvas.addEventListener('mousedown', this.keydown.bind(this), false );
        ig.system.canvas.addEventListener('mouseup', this.keyup.bind(this), false );
        ig.system.canvas.addEventListener('mousemove', this.mousemove.bind(this), false );

        ig.system.canvas.addEventListener( 'touchstart', this.touchEvent.bind( this ), false );
        ig.system.canvas.addEventListener( 'touchmove', this.touchEvent.bind( this ), false );
        ig.system.canvas.addEventListener( 'touchend', this.touchEvent.bind( this ), false );
        ig.system.canvas.addEventListener( 'touchcancel', this.touchEvent.bind( this ), false );
      }
    },

    // This is here for compatibility reasons.
    // You can still use the normal ig.input.state('click') or ig.input.mouse.x if you only need a single touch
    // but remember that this values could be the one of a random touch on your device
    
    keydown: function( e ) {
      this.parent( e );

      if ( e.type == 'mousedown' && !this.touches.mouse ) {
        this.touches.mouse = new TouchPoint( this.mouse.x, this.mouse.y, 'mouse', 'down' );
      }
    },

    keyup: function( e ) {
      this.parent( e );

      if ( e.type == 'mouseup' ) {
        var code = e.button == 2 ? ig.KEY.MOUSE1 : ig.KEY.MOUSE2;
        var action = this.bindings[code]

        if ( this.actions[action] ) return

        if ( this.touches.mouse ) {
          this.touches.mouse.state = 'up';
          this.touches.mouse.x = this.mouse.x;
          this.touches.mouse.y = this.mouse.y;
       
          this.delayedTouchUp.push( 'mouse' );
        }
      }
    },
    
    mousemove: function( e ) {
      this.parent( e );
      
      if ( this.state( 'click' ) && this.touches.mouse ) {

        this.touches.mouse.x = this.mouse.x;
        this.touches.mouse.y = this.mouse.y;
      }
    },

    clearPressed: function() {
      this.parent();
      this.ent = ig.game.entities;
    
      for ( var i = this.delayedTouchUp.length; i--; ) {
        delete this.touches[ ig.input.delayedTouchUp[ i ] ];
      }

      this.delayedTouchUp = [];
    
    this.multitouchState();
    },
  
  touchedEntity:function()
  {
    
    
    for(var i=0;i<this.ent.length;i++)
    {
      for ( var t in this.touches )
      {
              var touch_x = this.touches[t].x ;
              var touch_y = this.touches[t].y ;
        
            if(touch_x > this.ent[i].pos.x
            && touch_x < this.ent[i].pos.x + this.ent[i].size.x
            && touch_y > this.ent[i].pos.y 
            && touch_y < this.ent[i].pos.y + this.ent[i].size.y)
                {
            this.ent[i].clickedFinger = this.touches[t];
            this.ent[i].statedFinger = this.touches[t];
            
                    return this.ent[i];
                }
        else if(this.ent[i].statedFinger == this.touches[t])
        {
          this.ent[i].clickedFinger = null;
          this.ent[i].statedFinger = null;
          this.ent[i].isClicked = false;
        }
      }
    }
  },
    
    touchEvent: function( e ) {
      e.stopPropagation();
      e.preventDefault();

      var internalWidth = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth;
      var internalHeight= parseInt(ig.system.canvas.offsetHeight) || ig.system.realHeight;
      var scaleX = ig.system.scale * (internalWidth / ig.system.realWidth);
      var scaleY = ig.system.scale * (internalHeight / ig.system.realHeight);

      var pos = {left: 0, top: 0};
      if( ig.system.canvas.getBoundingClientRect ) {
        pos = ig.system.canvas.getBoundingClientRect();
      }
      
      for ( var i = e.changedTouches.length; i--; ) {
        var t = e.changedTouches[ i ];

        this[ 'multi' + e.type ](
          (t.clientX - pos.left) / scaleX,
          (t.clientY - pos.top) / scaleY,
          t.identifier
        );
      }
    },
  
  multitouchState:function() {
    for(var i=0;i<this.ent.length;i++)
    {
      for ( var t in this.touches )
      {
              var touch_x = this.touches[t].x ;
              var touch_y = this.touches[t].y ;
        
            if(touch_x > this.ent[i].pos.x
            && touch_x < this.ent[i].pos.x + this.ent[i].size.x
            && touch_y > this.ent[i].pos.y 
            && touch_y < this.ent[i].pos.y + this.ent[i].size.y)
                {
            this.ent[i].clickedFinger = this.touches[t];
            this.ent[i].statedFinger = this.touches[t];
                         if(typeof(this.ent[i].clicking) == 'function')
                         {
                                this.ent[i].clicking();
                         }
                }
        else if(this.ent[i].statedFinger == this.touches[t])
        {
          this.ent[i].clickedFinger = null;
          this.ent[i].statedFinger = null;
          this.ent[i].isClicked = false;
        }
      }
            
            if(!this.fingerCount.length)
            {
                if(this.ent[i].clickedFinger != null)
                {
                    this.ent[i].clickedFinger = null;
                }
                if(this.ent[i].statedFinger != null)
                {
                    this.ent[i].statedFinger = null;
                }
        this.ent[i].isClicked = false;
            }
    }
  },

    multitouchstart: function( x, y, id ) {
   /* pressed */
      var action = this.bindings[ ig.KEY.MOUSE1 ];
      if ( action ) {
        this.actions[action] = true;
        this.presses[action] = true;
      }

      this.touches[ id ] = new TouchPoint( x+ig.game.screen.x, y+ig.game.screen.y, id, 'down' );
      this.fingerCount.push( id );
      this.touches[id].pressed = true;
    
    var highestIndex = -1 ;
        
    for(var i=0;i<this.ent.length;i++)
    {
      for ( var t in this.touches )
      {
              var touch_x = this.touches[t].x ;
              var touch_y = this.touches[t].y ;
        
            if(touch_x > this.ent[i].pos.x
            && touch_x < this.ent[i].pos.x + this.ent[i].size.x
            && touch_y > this.ent[i].pos.y 
            && touch_y < this.ent[i].pos.y + this.ent[i].size.y)
                {
            this.ent[i].clickedFinger = this.touches[t];
                        if(typeof(this.ent[i].clicked) == 'function')
                        {
                            this.ent[i].clicked();
                            break;
                        }
                }
      }
    }
    },

    multitouchmove: function( x, y, id ) {
      if ( this.touches[ id ] ) {
        this.touches[ id ].x = x + ig.game.screen.x;
        this.touches[ id ].y = y + ig.game.screen.y;
      }
    },

    multitouchend: function( x, y, id ) {
    /* released */
      if ( this.touches[ id ] ) {
        this.touches[ id ].state = 'up';
        this.delayedTouchUp.push( id );

        var action = this.bindings[ ig.KEY.MOUSE1 ];
        if ( action && this._isEmpty( this.touches ) ) {
          this.delayedKeyup[ action ] = true;
        }
      }
      this.fingerCount.pop();
      this.touches[id].released = true;
    /*
    if(this.touchedEntity() != null && this.touchedEntity().clickedFinger == this.touches[id])
    {
       if(typeof(this.touchedEntity().released) == 'function')
     {
       this.touchedEntity().released();
     }
    }
    */
    
    
    for(var i=0;i<this.ent.length;i++)
    {
      for ( var t in this.touches )
      {
              var touch_x = this.touches[t].x ;
              var touch_y = this.touches[t].y ;
        
            if(touch_x > this.ent[i].pos.x
            && touch_x < this.ent[i].pos.x + this.ent[i].size.x
            && touch_y > this.ent[i].pos.y 
            && touch_y < this.ent[i].pos.y + this.ent[i].size.y)
                {
            if(this.ent[i].clickedFinger == this.touches[id])
            {
                            if(typeof(this.ent[i].released) == 'function')
                            {
                                this.ent[i].released();
                            }
            }
                }
      }
    }
    },

    multitouchcancel: function( x, y, id ) {
        this.multitouchend(x, y, id);
    },

    _isEmpty: function( obj ) {
      for ( var i in obj ) return false;
      return true;
    }

  });

});