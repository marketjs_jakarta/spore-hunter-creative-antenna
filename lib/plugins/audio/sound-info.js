/**
 *  SoundHandler
 *
 *  Created by Justin Ng on 2014-08-19.
 *  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
 */

ig.module('plugins.audio.sound-info')
.requires(
)
.defines(function () {

    SoundInfo = ig.Class.extend({
		FORMATS:{
			OGG:".ogg",
			MP3:".mp3",
		},
        
		/**
		* Define your sounds here
		* 
        */
		sfx:{
			kittyopeningSound:{path:"media/audio/opening/kittyopening"},
			staticSound:{path:"media/audio/play/static"},
			openingSound:{path:"media/audio/opening/opening"},
			playerShoot:{path:"media/audio/player-shoot"},
			buttonClick:{path:"media/audio/button-click"},
			enemyPop:{path:"media/audio/enemy-pop"},
			smallExplosion:{path:"media/audio/small-explosion"},
			laser1:{path:"media/audio/laser1"},	// https://audiojungle.net/item/laser/23369393
			hitIron:{path:"media/audio/hit-iron"},	// https://audiojungle.net/item/hit-iron/23185405
			upgrade:{path:"media/audio/upgrade"},
			hit:{path:"media/audio/hit"},
			revive:{path:"media/audio/revive"},
			fixing1:{path:"media/audio/fixing1"},
			fixing2:{path:"media/audio/fixing2"},
			fixing3:{path:"media/audio/fixing3"},
			explode:{path:"media/audio/explode"},
			smallBonus:{path:"media/audio/small-bonus"}
		},
		
        /**
        * Define your BGM here
        */
		bgm:{
			background:{path:'media/audio/bgm',startOgg:0,endOgg:21.463,startMp3:0,endMp3:21.463}
		}
        
		
    });

});
