ig.module('plugins.splash-loader')
.requires(
    'impact.loader',
    'impact.animation'
)
.defines(function() {
    ig.SplashLoader = ig.Loader.extend({
        desktopCoverDIVID: "play-desktop",

        splashDesktop: new ig.Image('media/graphics/splash/desktop/cover.png'),
        splashMobile: new ig.Image('media/graphics/splash/mobile/cover.png'),

        barFrame: new ig.Image('media/graphics/splash/loading/loading-bar-frame.png'),
        barComplete: new ig.Image('media/graphics/splash/loading/loading-bar-fill.png'),
        barSize: {
    			x: 515,
    			y: 114
    		},

        init:function(gameClass,resources){

            this.parent(gameClass,resources);

            // ADS
            ig.apiHandler.run("MJSPreroll");
        },

        end:function(){
            this.parent();
            this._drawStatus = 1;
            this.draw();

            if(ig.ua.mobile)
            {
                var play = ig.domHandler.getElementById("#play");
                ig.domHandler.show(play);
            }
            else {
                // desktop
                this.tapToStartDiv();
                return;
            }
            ig.system.setGame(MyGame);

            // CLEAR CUSTOM ANIMATION TIMER
            // window.clearInterval(ig.loadingScreen.animationTimer);
        },

        tapToStartDiv:function( onClickCallbackFunction ){
            this.desktopCoverDIV = document.getElementById(this.desktopCoverDIVID);

            // singleton pattern
            if ( this.desktopCoverDIV ) {
                return;
            }

            /* create DIV */
            this.desktopCoverDIV = document.createElement("div");
            this.desktopCoverDIV.id = this.desktopCoverDIVID;
            this.desktopCoverDIV.setAttribute("class", "play");
            this.desktopCoverDIV.setAttribute("style", "position: absolute; display: block; z-index: 999999; background-color: rgba(23, 32, 53, 0.7); visibility: visible; font-size: 10vmin; text-align: center; vertical-align: middle; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;");
            this.desktopCoverDIV.innerHTML = "<div style='color:white;background-color: rgba(255, 255, 255, 0.3); border: 2px solid #fff; font-size:20px; border-radius: 5px; position: relative; float: left; top: 50%; left: 50%; transform: translate(-50%, -50%);'><div style='padding:20px 50px; font-family: porkys;'>" + _STRINGS["Splash"]["TapToStart"] + "</div></div>";


            /* inject DIV */
            var parentDIV = document.getElementById("play").parentNode || document.getElementById("ajaxbar");
            parentDIV.appendChild(this.desktopCoverDIV);

            /* reize DIV */
            try {
                if ( typeof (ig.sizeHandler) !== "undefined" ) {
                    if ( typeof (ig.sizeHandler.coreDivsToResize) !== "undefined" ) {
                        ig.sizeHandler.coreDivsToResize.push( ("#"+this.desktopCoverDIVID) );
                        if ( typeof (ig.sizeHandler.reorient) === "function" ) {
                            ig.sizeHandler.reorient();
                        }
                    }
                }
                else if ( typeof (coreDivsToResize) !== "undefined" ) {
                    coreDivsToResize.push(this.desktopCoverDIVID);
                    if ( typeof (sizeHandler) === "function" ) {
                        sizeHandler();
                    }
                }
            } catch (error) {
                console.log(error);
            }


            /* click DIV */
            this.desktopCoverDIV.addEventListener("click", function(){

                /* play audio */
                try {

                    if ( typeof (ig.soundHandler) !== "undefined" ) {

                        /* resume audio context */
                        if ( typeof ( ig.soundHandler.bgmPlayer ) !== "undefined" ) {
                            if ( typeof ( ig.soundHandler.bgmPlayer.webaudio ) !== "undefined" ) {
                                if ( typeof ( ig.soundHandler.bgmPlayer.webaudio.context ) !== "undefined" ) {
                                    ig.soundHandler.bgmPlayer.webaudio.context.resume();
                                }
                            }
                        }
                        else {
                            /* re-instantiate sound handler */
                            ig.soundHandler = null;
                            if ( typeof (ig.soundList) !== "undefined" ) {
                                ig.soundHandler = new ig.SoundHandler( ig.soundList );
                            }
                            else {
                                ig.soundHandler = new ig.SoundHandler();
                            }
                        }

                        /* play static audio */
                        if ( typeof ( ig.soundHandler.sfxPlayer ) !== "undefined" ) {
                            if ( typeof ( ig.soundHandler.sfxPlayer.play ) === "function" ) {
                                ig.soundHandler.sfxPlayer.play("staticSound");
                            }
                        }

                        else if ( typeof ( ig.soundHandler.staticSound ) !== "undefined" ) {
                            if ( typeof ( ig.soundHandler.staticSound.play ) === "function" ) {
                                ig.soundHandler.staticSound.play();
                            }
                        }

                        else if ( typeof ( ig.soundHandler.playSound ) === "function" ) {
                            ig.soundHandler.playSound("staticSound");
                        }
                    }

                    else if ( typeof (Howl) !== "undefined" ) {
                        ig.global.staticSound = new Howl({src:['media/audio/play/static.ogg','media/audio/play/static.mp3']});
                        ig.global.staticSound.play();
                    }

                    else if ( typeof ( createjs ) !== "undefined" ) {
                        if ( typeof ( createjs.Sound ) !== "undefined" && typeof ( createjs.Sound.play ) === "function" ) {
                            createjs.Sound.play('opening');
                        }
                    }

                } catch (error) {
                    console.log(error);
                }

                /* hide DIV */
                this.setAttribute("style", "visibility: hidden;");

                /* end function */
                if ( typeof (onClickCallbackFunction) === "function" ) {
                    onClickCallbackFunction();
                }

                /* play game */
                ig.system.setGame(MyGame);
            });
        },

        drawCheck: 0,
        draw: function() {
            // Font Preload
			ig.system.context.font = "1px porkys";
            ig.system.context.fillText('porkys', -100, -100);
            
            this._drawStatus += (this.status - this._drawStatus)/5;

            //Check the game screen. see if the font are loaded first. Removing the two lines below is safe :)
            if(this.drawCheck === 1) console.log('Font should be loaded before loader draw loop');
            if(this.drawCheck < 2) this.drawCheck ++;


            // CLEAR RECTANGLE
            ig.system.context.fillStyle = '#000';
            ig.system.context.fillRect( 0, 0, ig.system.width, ig.system.height );

            var s = ig.system.scale,
              rWidth = ig.system.realWidth,
              rHeight = ig.system.realHeight,
              rHeightH = rHeight / 2,
      				rWidthH = rWidth / 2,
      				posX = rWidthH - this.barSize.x / 2,
      				posY = rHeight - 325;

            if(ig.ua.mobile){
                this.splashMobile.draw(0,0);
            }else{
                this.splashDesktop.draw(0,0);
            }

            // Scale equal to system.scale
      			var widthBar = this.barSize.x * this._drawStatus,
              scaledBarWidth = widthBar * s,
      				scaledBarHeight = this.barSize.y,
      				scaledPosX = posX * s,
      				scaledPosY = posY * s;

            // Draw Empty Bar
      			this.barFrame.draw(scaledPosX, scaledPosY);

            // Draw Fill Bar
      			if(widthBar > 0) {
      				this.barComplete.draw(scaledPosX, scaledPosY, 0, 0, scaledBarWidth, scaledBarHeight);
      			}
        }
    });
});
