/**
 * Utility library by Kevin Andrean
 * 
 * Compiled operation that we often uses
 * 
 * 
 */
ig.module('plugins.kvn-utility')
    .requires(
        'impact.entity'
    ).defines(function () {

        ig.KvnUtility = {

            // ------------------ Math Operation --------------------------\\

            /**
             * Returns a random number between min (included) and max (excluded)
             * @param {number} min 
             * @param {number} max 
             */
            getRandomInteger: function (min, max) {
                return Math.floor(Math.random() * (max - min)) + min;
            },

            /**
             * Return given number with positif or negative value (random)
             * @param {number} val 
             */
            getRandomNegative: function (val) {
                return val *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;
            },

            /**
             * Return value between start and end at amt percentage
             * @param {number} start 
             * @param {number} end 
             * @param {number} amt between 0 - 1
             */
            lerp: function (start, end, amt) {
                return (1 - amt) * start + amt * end;
            },

            // -------------------- Text Operation ----------------------\\

            /**
             * Draw stroked text.
             * @param {*} ctx canvas context
             * @param {string} text 
             * @param {number} x position
             * @param {number} y position
             * @param {*} strokeStyle default black
             * @param {*} lineWidth default 4
             */
            drawStrokedText: function (ctx, text, x, y, strokeStyle, lineWidth) {
                // using the solutions from @Simon Sarris and @Jackalope from
                // https://stackoverflow.com/questions/7814398/a-glow-effect-on-html5-canvas

                // Setup parameter default value
                if (strokeStyle == null) strokeStyle = 'black';
                if (lineWidth == null) lineWidth = 4;

                ctx.save();
                ctx.strokeStyle = strokeStyle;
                ctx.lineWidth = lineWidth;
                ctx.lineJoin = "round";
                ctx.miterLimit = 2;
                ctx.strokeText(text, x, y);
                ctx.fillText(text, x, y);
                ctx.restore();
            },

            /**
             * Draw shadowed text
             * @param {*} ctx canvas context
             * @param {*} text 
             * @param {*} x position
             * @param {*} y position
             * @param {*} shadowBlur default 3
             * @param {*} shadowOffsetX default 4
             * @param {*} shadowOffsetY default 4
             */
            drawShadowedText: function(ctx, text, x, y, shadowColor, shadowBlur, shadowOffsetX, shadowOffsetY) {
                // Setup parameter default value
                if (shadowColor == null) shadowColor = '#000000';
                if (shadowBlur == null) shadowBlur = 3;
                if (shadowOffsetX == null) shadowOffsetX = 4;
                if (shadowOffsetY == null) shadowOffsetY = 4;

                ctx.save();
                ctx.shadowBlur = shadowBlur;
                ctx.shadowColor = shadowColor;
                ctx.shadowOffsetX = shadowOffsetX;
                ctx.shadowOffsetY = shadowOffsetY;
                ctx.fillText(text, x, y);
                ctx.restore();
            },

            /**
             * Draw glowing text
             * @param {*} ctx canvas context
             * @param {*} text 
             * @param {*} x position
             * @param {*} y position
             * @param {*} glowColorHexString 
             * @param {*} glowDistance default 10
             */
            drawGlowingText: function (ctx, text, x, y, glowColorHexString, glowDistance) {
                // Setup parameter default value
                if (glowDistance == null) glowDistance = 10;

                ctx.save();
                ctx.shadowBlur = glowDistance;
                ctx.shadowColor = glowColorHexString;
                ctx.strokeText(text, x, y);

                for (var i = 0; i < 3; i++){
                    ctx.fillText(text, x, y); //seems to be washed out without 3 fills
                }

                ctx.restore();
            },

            /**
             * 
             * @param {*} ctx canvas context
             * @param {*} text 
             * @param {*} x 
             * @param {*} y 
             * @param {*} blur default 5
             */
            drawBlurredText: function (ctx, text, x, y, blur) {
                //using technique from https://www.html5rocks.com/en/tutorials/canvas/texteffects/

                // Setup parameter default value
                if (blur == null) blur = 5;

                ctx.save();
                var width = ctx.measureText(text).width + blur * 2;
                ctx.shadowColor = ctx.fillStyle;
                ctx.shadowOffsetX = width + x + ctx.canvas.width;
                ctx.shadowOffsetY = 0;
                ctx.shadowBlur = blur;
                ctx.fillText(text, -width + -ctx.canvas.width, y);
                ctx.restore();
            },

            /**
             * 
             * @param {*} ctx canvas context
             * @param {*} text 
             * @param {*} x 
             * @param {*} y 
             * @param {*} reflectionScale 
             * @param {*} reflectionAlpha 
             */
            drawReflectedText: function (ctx, text, x, y, reflectionScale, reflectionAlpha) {
                // Setup parameter default value
                if (reflectionScale == null) reflectionScale = 0.2;
                if (reflectionAlpha == null) reflectionAlpha = 0.10;

                ctx.save();
                ctx.fillText(text, x, y);
                ctx.scale(1, -reflectionScale);
                ctx.globalAlpha = reflectionAlpha;
                ctx.shadowColor = ctx.fillStyle;
                ctx.shadowBlur = 15;
                ctx.fillText(text, x, -(y * (1 / reflectionScale)));
                ctx.restore();
            }
        };
    });